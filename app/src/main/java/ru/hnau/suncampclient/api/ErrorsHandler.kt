package ru.hnau.suncampclient.api

import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.utils.showToast
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.takeIfPositive
import ru.hnau.suncamp.common.api.error.ApiError
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType
import ru.hnau.suncampclient.pages.new_version.NewVersionInfo
import ru.hnau.suncampclient.pages.new_version.NewVersionPage
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.CountSuffixManager
import ru.hnau.suncampclient.utils.LoginManager
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager


object ErrorsHandler {

    fun handle(ex: Exception) {
        if (ex is ApiErrorException) {
            handleApiError(ex.apiError)
            return
        }

        ex.message?.let { showToast(it.toGetter()) }

    }

    private fun handleApiError(apiError: ApiError) =
            when (apiError.type) {
                ApiErrorType.UNSUPPORTED_VERSION -> onUnsupportedVersion(apiError)
                ApiErrorType.NEED_LOGIN -> onNeedLogin(apiError)
                ApiErrorType.IP_BLOCKED -> onBlockedIp(apiError)
                else -> showToast(apiError.text.toGetter())
            }

    private fun onNeedLogin(apiError: ApiError) {
        showToast(apiError.text.toGetter())
        LoginManager.logout()
    }

    private fun onUnsupportedVersion(apiError: ApiError) {
        val newVersionInfo = NewVersionInfo(
                hard = true,
                description = "Данная версия приложения более не поддерживается, необходимо обновить приложение",
                updateUrl = apiError.params?.get("updateUrl")
        )
        NewVersionPage.show(newVersionInfo)
    }

    private fun onBlockedIp(apiError: ApiError) {
        DialogManager.show<Unit> {
            val seconds = apiError.params?.get("seconds")?.toIntOrNull()
            val text = "IP адрес заблокирован " +
                    (seconds?.let { "на ${secondsToText(it)} " } ?: "") +
                    "за чрезмерную активность"

            DialogManager.show<Unit> {
                title(text.toGetter())
                button(
                        text = "Закрыть".toGetter(),
                        onClick = { Possible.success() },
                        color = SunButtonColorInfo.WHITE_ON_RED,
                        shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
                        size = SunButtonSizeInfo.SIZE_16
                )
            }
        }
    }

    private fun secondsToText(seconds: Int): String {
        val sec = seconds % 60
        val secSuffix = CountSuffixManager.getSuffix(
                count = sec,
                forCount1 = "секунду",
                forCount2_4 = "секунды",
                forOther = "секунд"
        )
        val secWithSuffix = "$sec $secSuffix"

        val min = seconds / 60
        val minWithSuffix = min.takeIfPositive()?.let {
            val minSuffix = CountSuffixManager.getSuffix(
                    count = it,
                    forCount1 = "минуту",
                    forCount2_4 = "минуты",
                    forOther = "минут"
            )

            "$it $minSuffix"
        }

        return (minWithSuffix?.let { "$it " } ?: "") + secWithSuffix
    }

}