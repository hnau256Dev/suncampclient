package ru.hnau.suncampclient.api

import android.util.Log
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import ru.hnau.androidutils.utils.mapUi
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.finisher.NewThreadFinisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.tryOrElse
import ru.hnau.suncamp.common.api.ApiResult
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.possible
import ru.hnau.suncamp.common.api.response.SubordinateStorageInfo
import ru.hnau.suncamp.common.data.*
import ru.hnau.suncampclient.utils.LoginManager


interface Api {

    companion object {

        private val TAG = Api::class.java.simpleName

        private val VERSION = 4

        fun userRegister(
                login: String,
                password: String,
                name: String
        ) =
                callToFinisher(api.userRegister(login, password, name)).compose()

        fun userLogin(
                login: String,
                password: String
        ) =
                callToFinisher(api.userLogin(login, password)).compose()

        fun userGetAll() =
                callToFinisher(api.userGetAll()).compose()

        fun cardGetList() =
                callToFinisher(api.cardGetList()).compose()

        fun cardRegister(
                uuid: String
        ) =
                callToFinisher(api.cardRegister(uuid)).compose()

        fun cardRemove(
                uuid: String
        ) =
                callToFinisher(api.cardRemove(uuid)).compose()

        fun cardGetUserByCardUuid(uuid: String) =
                callToFinisher(api.cardGetUserByCardUuid(uuid)).compose()

        fun balanceGet(
                login: String = LoginManager.login
        ) =
                callToFinisher(api.balanceGet(login)).compose()

        fun balanceGetOfUsers() =
                callToFinisher(api.balanceGetOfUsers()).compose()

        fun storageManagerGetSubordinateList() =
                callToFinisher(api.storageManagerGetSubordinateList()).compose()

        fun transactionGetList(
                login: String = LoginManager.login
        ) =
                callToFinisher(api.transactionGetList(login)).compose()

        fun transactionExecute(
                fromUserLogin: String,
                toUserLogin: String,
                amount: Long,
                category: TransactionCategory,
                comment: String
        ) =
                callToFinisher(api.transactionExecute(fromUserLogin, toUserLogin, amount, category, comment)).compose()

        fun transactionFromStorageToUsers(
                fromUserLogin: String,
                toUsers: Set<User>,
                amount: Long,
                category: TransactionCategory,
                comment: String
        ) =
                callToFinisher(api.transactionFromStorageToUsers(fromUserLogin, usersToList(toUsers), amount, category, comment)).compose()

        fun transactionFromUsersToStorage(
                fromUsers: Set<User>,
                toUserLogin: String,
                amount: Long,
                category: TransactionCategory,
                comment: String
        ) =
                callToFinisher(api.transactionFromUsersToStorage(usersToList(fromUsers), toUserLogin, amount, category, comment)).compose()

        private fun usersToList(users: Iterable<User>) =
                users.joinToString(
                        separator = ",",
                        transform = User::login
                )

        private fun <T : Any> Finisher<Possible<ApiResult<T>>>.compose() = map { possibleApiResult ->

            val possibleResult = possibleApiResult.tryMapPossible {
                it.handle(
                        onSuccess = {
                            Possible.success(it)
                        },
                        onError = {
                            Possible.error(ApiErrorException(it))
                        }
                )
            }

            possibleResult.ifError {
                ErrorsHandler.handle(it as? ApiErrorException
                        ?: Exception("Ошибка связи с сервером"))
            }

            possibleResult
        }


        private val api = run {

            val retrofit = Retrofit.Builder()
                    .baseUrl("http://hnau.ru:8080/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            retrofit.create(Api::class.java)
        }

        private fun <T : Any> callToFinisher(call: Call<T>) =
                NewThreadFinisher.sync {
                    val url = call.request().url()
                    Log.d(TAG, "[$url] Start")
                    tryOrElse(
                            throwsAction = {
                                val result = call.execute()
                                if (!result.isSuccessful) {
                                    val message = result.errorBody().string()
                                    Log.d(TAG, "[$url] Error: $message")
                                    Possible.error<T>(message)
                                } else {
                                    val content = result.body()
                                    Log.d(TAG, "[$url] Success: $content")
                                    Possible.success(content)
                                }
                            },
                            onThrow = {
                                Log.e(TAG, "[$url] Unexpected error", it)
                                Possible.error(it)
                            }
                    )
                }.mapUi()

    }

    @GET("user/register")
    fun userRegister(
            @Query("login") login: String,
            @Query("password") password: String,
            @Query("name") name: String,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<Unit>>

    @GET("user/login")
    fun userLogin(
            @Query("login") login: String,
            @Query("password") password: String,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<LoginResult>>

    @GET("user/get-all")
    fun userGetAll(
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<List<User>>>

    @GET("card/get-list")
    fun cardGetList(
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<List<Card>>>

    @GET("card/register")
    fun cardRegister(
            @Query("cardUuid") cardUuid: String,
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<Unit>>

    @GET("card/remove")
    fun cardRemove(
            @Query("cardUuid") cardUuid: String,
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<Unit>>

    @GET("card/get-user-by-card-uuid")
    fun cardGetUserByCardUuid(
            @Query("cardUuid") cardUuid: String,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<User>>

    @GET("balance/get")
    fun balanceGet(
            @Query("login") login: String,
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<Long>>

    @GET("balance/get-of-users")
    fun balanceGetOfUsers(
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<List<Pair<String, Long>>>>

    @GET("transaction/get-list")
    fun transactionGetList(
            @Query("login") login: String,
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<List<Transaction>>>

    @GET("transaction/execute")
    fun transactionExecute(
            @Query("fromUserLogin") fromUserLogin: String,
            @Query("toUserLogin") toUserLogin: String,
            @Query("amount") amount: Long,
            @Query("category") category: TransactionCategory,
            @Query("comment") comment: String,
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<Transaction>>

    @GET("transaction/from-storage-to-users")
    fun transactionFromStorageToUsers(
            @Query("fromStorageUserLogin") fromStorageUserLogin: String,
            @Query("toUsersLogins") toUsersLogins: String,
            @Query("amount") amount: Long,
            @Query("category") category: TransactionCategory,
            @Query("comment") comment: String,
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<Unit>>

    @GET("transaction/from-users-to-storage")
    fun transactionFromUsersToStorage(
            @Query("fromUsersLogins") fromUsersLogins: String,
            @Query("toStorageUserLogin") toStorageUserLogin: String,
            @Query("amount") amount: Long,
            @Query("category") category: TransactionCategory,
            @Query("comment") comment: String,
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<Unit>>

    @GET("storage-manager/get-subordinate-list")
    fun storageManagerGetSubordinateList(
            @Query("authToken") authToken: String = LoginManager.authToken,
            @Query("version") version: Int = VERSION
    ): Call<ApiResult<List<SubordinateStorageInfo>>>

}