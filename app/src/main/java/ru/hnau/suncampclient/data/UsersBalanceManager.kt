package ru.hnau.suncampclient.data

import ru.hnau.jutils.finisher.mapPossible
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncampclient.api.Api


object UsersBalanceManager : SunCampDataManager<Possible<Map<String, Long>>>(
        {
            Api.balanceGetOfUsers().mapPossible { data ->
                data.associate { it }
            }
        }
)