package ru.hnau.suncampclient.data

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.producer.async.GetterAsyncDataProducer
import ru.hnau.suncampclient.utils.LoginManager


open class SunCampDataManager<T : Any>(
        finisherProducer: () -> Finisher<T>
) : GetterAsyncDataProducer<T>(
        action = { onFinished -> finisherProducer.invoke().await(onFinished) },
        finisherLifetime = TimeValue.MINUTE * 10
) {

    init {
        LoginManager.onUserChangedProducer.attach { invalidate() }
    }

}