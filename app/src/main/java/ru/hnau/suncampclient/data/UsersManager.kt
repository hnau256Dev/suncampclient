package ru.hnau.suncampclient.data

import ru.hnau.jutils.finisher.mapPossible
import ru.hnau.jutils.finisher.mapPossibleOrError
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.api.Api


object UsersManager : SunCampDataManager<Possible<List<User>>>(
        { Api.userGetAll().mapPossible { users -> users.sortedBy { it.name } } }
) {

    fun getByLogin(login: String) =
            currentData.mapPossibleOrError { users ->
                Possible.trySuccess(users.find { it.login == login })
            }

}