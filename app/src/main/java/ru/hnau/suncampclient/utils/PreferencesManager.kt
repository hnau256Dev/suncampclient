package ru.hnau.suncampclient.utils

import android.content.SharedPreferences
import ru.hnau.androidutils.preferences.PreferencesStringProperty
import ru.hnau.androidutils.utils.ContextConnector
import ru.hnau.jutils.getter.Getter


object PreferencesManager {

    private val preferences = Getter<SharedPreferences> {
        ContextConnector.context.getSharedPreferences("PREFERENCES", 0)
    }

    var userLogin: String by PreferencesStringProperty(preferences.get(), "USER_LOGIN")

}