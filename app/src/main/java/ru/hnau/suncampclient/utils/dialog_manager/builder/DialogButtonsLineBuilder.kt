package ru.hnau.suncampclient.utils.dialog_manager.builder

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncampclient.ui.view.sun_button.SunButton
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.SizeManager
import java.util.*


class DialogButtonsLineBuilder<T : Any>(
        val context: Context,
        val onNeedFinishDialog: (result: Possible<T>) -> Unit
) {


    private val contentViews = LinkedList<View>()


    fun button(
            text: StringGetter,
            onClick: () -> Possible<T>?,
            size: SunButtonSizeInfo = SunButtonSizeInfo.SIZE_16,
            color: SunButtonColorInfo = SunButtonColorInfo.BROWN_ON_YELLOW,
            shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT
    ) {
        contentViews.add(
                SunButton(
                        context = context,
                        shadowInfo = shadowInfo,
                        color = color,
                        size = size,
                        text = text,
                        onClick = {
                            val onClickResult = onClick.invoke()
                            onClickResult?.let(onNeedFinishDialog::invoke)
                        }
                ).apply {
                    setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
                }
        )
    }

    fun createView() =
            when (contentViews.size) {
                0 -> null
                1 -> contentViews[0]
                else -> LinearLayout(context).apply {
                    orientation = LinearLayout.HORIZONTAL
                    gravity = Gravity.CENTER
                    contentViews.forEach(::addView)
                }
            }

}