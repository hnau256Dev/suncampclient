package ru.hnau.suncampclient.utils.income_manager

import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.utils.showToast
import ru.hnau.jutils.finisher.awaitSuccess
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.MyCardsManager
import ru.hnau.suncampclient.pages.cards.CardsPage
import ru.hnau.suncampclient.ui.view.qr_code.QrCodeView
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager


object IncomeManager {

    fun doIncome() {

        MyCardsManager.currentData
                .apply(AppActivityConnector::waitFinisher)
                .awaitSuccess { cards ->
                    when (cards.size) {
                        0 -> showToast(StringGetter(R.string.income_manager_error_no_cards))
                        1 -> DialogManager.show<Unit> { view(QrCodeView(context, cards[0].uuid)) }
                        else -> AppActivityConnector.showPage(CardsPage())
                    }
                }

    }

}