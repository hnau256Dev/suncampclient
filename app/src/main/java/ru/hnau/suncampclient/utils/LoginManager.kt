package ru.hnau.suncampclient.utils

import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.SimpleProducer
import ru.hnau.suncamp.common.data.LoginResult
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.pages.auth.AuthPage
import ru.hnau.suncampclient.pages.auth.AuthPageInfo


object LoginManager {

    private var lastLoggedLogin: String? = null

    val login: String
        get() = PreferencesManager.userLogin

    private var loginResult: LoginResult? = null

    val authToken: String
        get() = loginResult?.authToken ?: ""

    val user: User?
        get() = loginResult?.user

    private val onUserChangedProducerInner = SimpleProducer<String>()

    val onUserChangedProducer: Producer<String>
        get() = onUserChangedProducerInner


    fun onLogged(loginResult: LoginResult) {
        this.loginResult = loginResult
        onLogged(loginResult.user.login)
    }

    private fun onLogged(newLogin: String) {
        if (lastLoggedLogin != null && newLogin != lastLoggedLogin) {
            onUserChangedProducerInner.callListeners(newLogin)
        }
        lastLoggedLogin = newLogin
    }

    fun logout() {
        synchronized(this) {
            loginResult ?: return
            loginResult = null
        }
        AppActivityConnector.showPage(AuthPage(login, AuthPageInfo.FOR_NEED_LOGIN_ERROR))
    }

}