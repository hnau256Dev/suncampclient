package ru.hnau.suncampclient.utils.dialog_manager

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawer
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawerRectShape
import ru.hnau.androidutils.ui.utils.shadow.info.ShadowInfo
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement
import ru.hnau.androidutils.ui.view.utils.layout
import ru.hnau.androidutils.ui.view.utils.makeMeasureSpec
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering


@SuppressLint("ViewConstructor")
class DialogDecorationView(
        private val contentView: View
) : ViewGroup(contentView.context) {

    companion object {

        private val SHADOW_INFO = ShadowInfo(
                offset = DpPxGetter.fromDp(16),
                blur = DpPxGetter.fromDp(24),
                color = ColorGetter.BLACK,
                alpha = 1f
        )

    }

    private val contentBackgroundPaint = Paint().apply {
        color = Color.WHITE
    }

    private val contentRect = Rect()

    private val shadowDrawer = ShadowDrawer(
            context = context,
            shadowInfo = SHADOW_INFO,
            shape = ShadowDrawerRectShape(contentRect)
    )

    init {
        setSoftwareRendering()
        addView(contentView)
    }

    override fun dispatchDraw(canvas: Canvas) {
        shadowDrawer.draw(canvas)
        canvas.drawRect(contentRect, contentBackgroundPaint)
        super.dispatchDraw(canvas)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val viewWidth = contentView.measuredWidth
        val viewHeight = contentView.measuredHeight

        val viewLeft = (shadowDrawer.shadowSizeRect.left + (width - shadowDrawer.shadowSizeRect.left - shadowDrawer.shadowSizeRect.right - viewWidth) / 2).toInt()
        val viewTop = (shadowDrawer.shadowSizeRect.top + (height - shadowDrawer.shadowSizeRect.top - shadowDrawer.shadowSizeRect.bottom - viewHeight) / 2).toInt()

        contentRect.set(
                viewLeft,
                viewTop,
                viewLeft + viewWidth,
                viewTop + viewHeight
        )

        contentView.layout(contentRect)

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val maxWidth = getMaxMeasurement(widthMeasureSpec, 0)
        val maxHeight = getMaxMeasurement(heightMeasureSpec, 0)
        val contentMaxWidth = (maxWidth - shadowDrawer.shadowSizeRect.left - shadowDrawer.shadowSizeRect.right).toInt()
        val contentMaxHeight = (maxHeight - shadowDrawer.shadowSizeRect.top - shadowDrawer.shadowSizeRect.bottom).toInt()

        contentView.measure(
                makeMeasureSpec(contentMaxWidth, MeasureSpec.AT_MOST),
                makeMeasureSpec(contentMaxHeight, MeasureSpec.AT_MOST)
        )

        setMeasuredDimension(maxWidth, maxHeight)
    }

}