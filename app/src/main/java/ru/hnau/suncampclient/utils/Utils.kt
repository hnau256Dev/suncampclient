package ru.hnau.suncampclient.utils

import android.graphics.Path
import android.graphics.RectF
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.list.group.ListGroup
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncampclient.R
import java.util.*


object Utils {

    const val PIN_LENGTH = 4

}

private const val ROUND_SIDE_WIDTH_BY_WIDTH = 0.015f

fun Path.initWithRoundSidesRect(rect: RectF) {

    val roundSideWidth = rect.width() * ROUND_SIDE_WIDTH_BY_WIDTH
    val roundSideYOffset = rect.height() / 4

    reset()

    moveTo(rect.left + roundSideWidth, rect.top)
    lineTo(rect.right - roundSideWidth, rect.top)
    cubicTo(
            rect.right + roundSideWidth / 4, rect.top + roundSideYOffset,
            rect.right + roundSideWidth / 4, rect.bottom - roundSideYOffset,
            rect.right - roundSideWidth, rect.bottom
    )

    lineTo(rect.left + roundSideWidth, rect.bottom)
    cubicTo(
            rect.left - roundSideWidth / 4, rect.bottom - roundSideYOffset,
            rect.left - roundSideWidth / 4, rect.top + roundSideYOffset,
            rect.left + roundSideWidth, rect.top
    )
}

private val userTypeSortAmount = hashMapOf(
        UserType.user to 1,
        UserType.manager to 2,
        UserType.storage to 3,
        UserType.admin to 4
)

private val userTypeTitle = hashMapOf(
        UserType.user to R.string.user_type_users_title,
        UserType.manager to R.string.user_type_managers_title,
        UserType.storage to R.string.user_type_storages_title,
        UserType.admin to R.string.user_type_admins_title
)

fun List<User>.splitToGroups() =
        this
                .groupBy { it.type }
                .toList()
                .sortedBy { userTypeSortAmount[it.first] }
                .map {
                    val title = StringGetter(userTypeTitle[it.first] ?: 0)
                    val usersList = it.second.sortedBy { it.name }
                    ListGroup(title, usersList)
                }