package ru.hnau.suncampclient.utils.dialog_manager.list

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.dp_px.DpPx
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.clickable.ClickableLinearLayout
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager


class DialogManagerListViewItemView(
        context: Context
) : ClickableLinearLayout(
        context = context,
        rippleDrawInfo = ColorManager.WHITE_RIPPLE_DRAW_INFO
), BaseListViewWrapper<DialogManagerListViewItem> {

    override val view: View
        get() = this

    private val iconView = ImageView(context).apply {
        setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            setRightMargin(SizeManager.DEFAULT_PADDING.get(context))
        }
    }

    private val titleView = Label(
            context = context,
            textColor = ColorManager.BROWN,
            minLines = 1,
            maxLines = 1,
            fontType = FontManager.PT_SANS,
            textSize = DpPxGetter.fromDp(20),
            gravity = HGravity.LEFT_CENTER_VERTICAL
    )

    private val subtitleView = Label(
            context = context,
            textColor = ColorManager.BROWN.mapWithAlpha(0.5f),
            minLines = 1,
            maxLines = 2,
            fontType = FontManager.PT_SANS,
            textSize = DpPxGetter.fromDp(12),
            gravity = HGravity.LEFT_CENTER_VERTICAL
    ).apply {
        setTopPadding(DpPxGetter.fromDp(4))
    }

    private val titlesView = LinearLayout(context).apply {
        orientation = VERTICAL
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f) {
            setRightMargin(SizeManager.DEFAULT_PADDING.get(context))
        }
        addView(titleView)
        addView(subtitleView)
    }

    private val arrowView = ImageView(context).apply {
        setImageResource(R.drawable.ic_arrow_right)
    }

    init {
        orientation = HORIZONTAL
        setPadding(SizeManager.DEFAULT_PADDING)
        gravity = Gravity.CENTER_VERTICAL
        addView(iconView)
        addView(titlesView)
        addView(arrowView)
    }

    override fun setContent(content: DialogManagerListViewItem) {

        content.icon?.let { icon ->
            iconView.visibility = View.VISIBLE
            iconView.setImageDrawable(icon.get(context))
        } ?: run { iconView.visibility = View.GONE }

        titleView.text = content.title

        content.subtitle?.let { subtitle ->
            subtitleView.visibility = View.VISIBLE
            subtitleView.text = subtitle
        } ?: run { subtitleView.visibility = View.GONE }

        setOnClickListener {
            DialogManager.hide()
            content.action.invoke()
        }

    }

}