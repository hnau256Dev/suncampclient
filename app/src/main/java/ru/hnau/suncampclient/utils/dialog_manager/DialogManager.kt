package ru.hnau.suncampclient.utils.dialog_manager

import ru.hnau.androidutils.ui.utils.ViewBasedManager
import ru.hnau.androidutils.utils.ContextConnector
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.error
import ru.hnau.jutils.possible.undefined
import ru.hnau.suncampclient.utils.dialog_manager.builder.DialogViewBuilder


object DialogManager : ViewBasedManager<DialogManagerView>() {

    override fun generateBaseView() = DialogManagerView(ContextConnector.context)

    fun <T : Any> show(
            viewBuilder: DialogViewBuilder<T>.() -> Unit
    ): Finisher<Possible<T>> {

        val dialogManagerView = this.baseView ?: return Finisher.forExistenceData(Possible.error())


        return Finisher { onFinished ->

            synchronized(this) {

                val dialogViewBuilder = DialogViewBuilder<T>(dialogManagerView.context) {
                    onFinished.invoke(it)
                    dialogManagerView.hideCurrentView()
                }

                viewBuilder.invoke(dialogViewBuilder)
                val view = dialogViewBuilder.createView()
                if (view == null) {
                    onFinished.error()
                    return@Finisher
                }

                dialogManagerView.showNewView(view) { onFinished.undefined() }

            }


        }

    }

    fun hide() {
        this.baseView?.hideCurrentView()
    }

}