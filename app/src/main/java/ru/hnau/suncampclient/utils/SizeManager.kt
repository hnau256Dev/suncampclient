package ru.hnau.suncampclient.utils

import ru.hnau.androidutils.context_getters.DpPxGetter


object SizeManager {

    val DEFAULT_PADDING = DpPxGetter.fromDp(16)

}