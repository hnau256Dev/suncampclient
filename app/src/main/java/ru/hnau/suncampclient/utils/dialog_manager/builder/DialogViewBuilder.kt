package ru.hnau.suncampclient.utils.dialog_manager.builder

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncampclient.ui.view.sun_button.SunButton
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager
import ru.hnau.suncampclient.utils.dialog_manager.list.DialogManagerListView
import ru.hnau.suncampclient.utils.dialog_manager.list.DialogManagerListViewItem
import java.util.*


class DialogViewBuilder<T : Any>(
        val context: Context,
        val onNeedFinishDialog: (result: Possible<T>) -> Unit
) {

    private var rawViewAdded = false

    private val contentViews = LinkedList<View>()

    private fun viewInner(view: View) {
        contentViews.add(view)
    }

    fun view(view: View) {
        rawViewAdded = true
        viewInner(view)
    }

    fun button(
            text: StringGetter,
            onClick: () -> Possible<T>?,
            size: SunButtonSizeInfo = SunButtonSizeInfo.SIZE_16,
            color: SunButtonColorInfo = SunButtonColorInfo.BROWN_ON_YELLOW,
            shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT
    ) =
            viewInner(
                    SunButton(
                            context = context,
                            shadowInfo = shadowInfo,
                            color = color,
                            size = size,
                            text = text,
                            onClick = {
                                val onClickResult = onClick.invoke()
                                onClickResult?.let(onNeedFinishDialog::invoke)
                            }
                    ).apply {
                        setMarginViewGroupLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
                    }
            )

    fun title(
            title: StringGetter,
            labelInfo: LabelInfo = LabelInfo(
                    fontType = FontManager.PT_SANS,
                    textColor = ColorManager.BROWN,
                    textSize = DpPxGetter.fromDp(24)
            )
    ) =
            text(title, labelInfo)

    fun text(
            text: StringGetter,
            labelInfo: LabelInfo = LabelInfo(
                    fontType = FontManager.PT_SANS,
                    textColor = ColorManager.BROWN,
                    textSize = DpPxGetter.fromDp(16)
            )
    ) =
            viewInner(
                    Label(context, text, labelInfo).apply {
                        setMarginViewGroupLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
                            val bottomMargin = SizeManager.DEFAULT_PADDING.getPxInt(context)
                            val horizontalMargin = bottomMargin / 2
                            setMargins(horizontalMargin, 0, horizontalMargin, bottomMargin)
                        }
                    }
            )

    fun buttons(buttonsBuilder: DialogButtonsLineBuilder<T>.() -> Unit) {
        val dialogButtonsLineBuilder = DialogButtonsLineBuilder(context, onNeedFinishDialog)
        buttonsBuilder.invoke(dialogButtonsLineBuilder)
        val view = dialogButtonsLineBuilder.createView() ?: return
        view.setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        viewInner(view)
    }

    fun list(items: List<DialogManagerListViewItem>) =
            view(DialogManagerListView(context, items))

    fun createView() =
            when (contentViews.size) {
                0 -> null
                1 -> contentViews[0]
                else -> LinearLayout(context).apply {
                    orientation = LinearLayout.VERTICAL
                    gravity = Gravity.CENTER
                    contentViews.forEach(::addView)
                }
            }?.apply {
                if (contentViews.size > 1 || !rawViewAdded) {
                    val paddingTop = SizeManager.DEFAULT_PADDING.getPxInt(context)
                    val paddingHorizontal = paddingTop / 2
                    val paddingBottom = paddingHorizontal / 2
                    setPadding(paddingHorizontal, paddingTop, paddingHorizontal, paddingBottom)
                }
            }

}