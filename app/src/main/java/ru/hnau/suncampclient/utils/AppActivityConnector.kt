package ru.hnau.suncampclient.utils

import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.androidutils.ui.view.pager.PagePagerConnector
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.suncampclient.ui.AppActivity


object AppActivityConnector {

    var appActivity: AppActivity? = null
        private set

    fun onAppActivityCreated(appActivity: AppActivity) {
        this.appActivity = appActivity
    }

    fun onAppActivityDestroyed() {
        this.appActivity = null
    }

    fun waitFinisher(finisher: Finisher<*>) {
        this.appActivity?.finishersLockedProducer?.waitFinisher(finisher)
    }

    fun showPage(
            page: Page<*>,
            clearStack: Boolean = false,
            precedingPages: List<Page<*>> = emptyList()
    ) =
            appActivity?.showPage(page, clearStack, precedingPages)?.let { true } ?: false

    fun goBack(
            pageResolver: (List<Page<*>>) -> Int? = PagePagerConnector.DEFAULT_GO_BACK_PAGE_RESOLVER
    ) =
            appActivity?.goBack(pageResolver)?.let { true } ?: false

}