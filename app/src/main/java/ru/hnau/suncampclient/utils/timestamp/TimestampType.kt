package ru.hnau.suncampclient.utils.timestamp

import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.suncampclient.R
import java.util.*


enum class TimestampType(
        val title: StringGetter,
        private val showingType: TimestampShowingType
) {

    TODAY(
            title = StringGetter(R.string.timestamp_type_title_today),
            showingType = TimestampShowingType.TIME
    ),

    YESTERDAY(
            title = StringGetter(R.string.timestamp_type_title_yesterday),
            showingType = TimestampShowingType.TIME
    ),

    THIS_WEEK(
            title = StringGetter(R.string.timestamp_type_title_this_week),
            showingType = TimestampShowingType.MONTH_AND_DAY
    ),

    THIS_MONTH(
            title = StringGetter(R.string.timestamp_type_title_this_month),
            showingType = TimestampShowingType.MONTH_AND_DAY
    ),

    THIS_YEAR(
            title = StringGetter(R.string.timestamp_type_title_this_year),
            showingType = TimestampShowingType.MONTH_AND_DAY
    ),

    LATER(
            title = StringGetter(R.string.timestamp_type_title_later),
            showingType = TimestampShowingType.DATE
    );

    companion object {


        fun choose(timestamp: Long): TimestampType {
            val now = Calendar.getInstance()
            val then = Calendar.getInstance().apply { timeInMillis = timestamp }

            if (now.get(Calendar.YEAR) > then.get(Calendar.YEAR)) {
                return LATER
            }

            if (now.get(Calendar.MONTH) > then.get(Calendar.MONTH)) {
                return THIS_YEAR
            }

            if (now.get(Calendar.DAY_OF_YEAR) > then.get(Calendar.DAY_OF_YEAR) + 6) {
                return THIS_MONTH
            }

            if (now.get(Calendar.DAY_OF_YEAR) > then.get(Calendar.DAY_OF_YEAR) + 1) {
                return THIS_WEEK
            }

            if (now.get(Calendar.DAY_OF_YEAR) > then.get(Calendar.DAY_OF_YEAR)) {
                return YESTERDAY
            }

            return TODAY
        }


        fun formatTimestamp(timestamp: Long) = choose(timestamp).formatTimestamp(timestamp)

    }

    fun formatTimestamp(timestamp: Long) = showingType.formatTimestamp(timestamp)

}