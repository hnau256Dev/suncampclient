package ru.hnau.suncampclient.utils


object CountSuffixManager {

    fun <R : Any> getSuffix(
            count: Int,
            forCount1: R,
            forCount2_4: R,
            forOther: R
    ) = when {
        count in 11..19 -> forOther
        count % 10 == 1 -> forCount1
        count % 10 in 2..4 -> forCount2_4
        else -> forOther
    }


}