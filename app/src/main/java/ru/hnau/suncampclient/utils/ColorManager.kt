package ru.hnau.suncampclient.utils

import android.content.Context
import android.graphics.Color
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.drawables.waiter.WaiterDrawableColor
import ru.hnau.androidutils.ui.drawables.waiter.WaiterDrawableSize
import ru.hnau.androidutils.ui.drawables.waiter.WaiterView
import ru.hnau.androidutils.ui.ripple.RippleDrawInfo
import ru.hnau.androidutils.ui.ripple.RippleInfo
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.utils.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.utils.shadow.info.ShadowInfo
import ru.hnau.androidutils.ui.view.header.HeaderInfo
import ru.hnau.androidutils.ui.view.header.back.button.HeaderBackButtonInfo
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.input.InputInfo


object ColorManager {

    val TRANSPARENT = ColorGetter.byColor(Color.TRANSPARENT)
    val YELLOW = ColorGetter.byResId(R.color.yellow)
    val ORANGE = ColorGetter.byResId(R.color.orange)
    val RED = ColorGetter.byResId(R.color.red)
    val BROWN = ColorGetter.byResId(R.color.brown)
    val WHITE = ColorGetter.byResId(R.color.white)
    val BACKGROUND_SHADOW = ColorGetter.byResId(R.color.background_shadow)

    val DEFAULT_RIPPLE_INFO = RippleInfo()

    val YELLOW_RIPPLE_DRAW_INFO = RippleDrawInfo(
            rippleInfo = DEFAULT_RIPPLE_INFO,
            color = BROWN,
            backgroundColor = YELLOW
    )

    val ORANGE_RIPPLE_DRAW_INFO = RippleDrawInfo(
            rippleInfo = DEFAULT_RIPPLE_INFO,
            color = BROWN,
            backgroundColor = ORANGE
    )

    val RED_RIPPLE_DRAW_INFO = RippleDrawInfo(
            rippleInfo = DEFAULT_RIPPLE_INFO,
            color = BROWN,
            backgroundColor = RED
    )

    val WHITE_RIPPLE_DRAW_INFO = RippleDrawInfo(
            rippleInfo = DEFAULT_RIPPLE_INFO,
            color = BROWN,
            backgroundColor = WHITE
    )

    val BROWN_YELLOW_RIPPLE_DRAW_INFO = RippleDrawInfo(
            rippleInfo = DEFAULT_RIPPLE_INFO,
            color = YELLOW,
            backgroundColor = BROWN
    )

    val INPUT_INFO = InputInfo(
            textSize = DpPxGetter.fromDp(24f),
            textColor = ColorManager.BROWN,
            backgroundColor = ColorManager.WHITE,
            paddingHorizontal = DpPxGetter.fromDp(16),
            paddingVertical = DpPxGetter.fromDp(12)
    )

    val LABEL_INFO_TITLE = LabelInfo(
            maxLines = 1,
            minLines = 1,
            textColor = YELLOW,
            textSize = DpPxGetter.fromDp(32),
            fontType = FontManager.PT_SANS,
            gravity = HGravity.CENTER
    )

    val SHADOW_INFO_NORMAL = ShadowInfo(
            color = ColorGetter.BLACK,
            alpha = 0.5f,
            blur = DpPxGetter.fromDp(8),
            offset = DpPxGetter.fromDp(4)
    )

    val SHADOW_INFO_PRESSED = ShadowInfo(
            color = ColorGetter.BLACK,
            alpha = 0.5f,
            blur = DpPxGetter.fromDp(4),
            offset = DpPxGetter.fromDp(2)
    )

    val BUTTON_SHADOW_INFO = ButtonShadowInfo(
            normal = SHADOW_INFO_NORMAL,
            pressed = SHADOW_INFO_PRESSED,
            animationTime = TimeValue.MILLISECOND * 100
    )

    val HEADER_INFO = HeaderInfo(
            backgroundColor = YELLOW
    )

    val HEADER_BACK_BUTTON_INFO = HeaderBackButtonInfo(
            color = BROWN
    )

    val HEADER_LABEL_INFO = LabelInfo(
            textColor = BROWN,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(20),
            fontType = FontManager.PT_SANS,
            gravity = HGravity.LEFT_CENTER_VERTICAL
    )

    fun createWaiter(
            context: Context,
            lockedProducer: LockedProducer,
            size: WaiterDrawableSize = WaiterDrawableSize.LARGE
    ) =
            WaiterView(
                    context = context,
                    lockedProducer = lockedProducer,
                    color = WaiterDrawableColor(
                            foreground = ORANGE,
                            background = ColorGetter.argb(0.5f, 0f, 0f, 0f)
                    ),
                    size = size
            )

}