package ru.hnau.suncampclient.utils

import ru.hnau.androidutils.ui.font_type.FontTypeGetter


object FontManager {

    val PT_SANS = FontTypeGetter("fonts/pt-sans.ttf")

}