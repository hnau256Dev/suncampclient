package ru.hnau.suncampclient.ui.view.qr_code

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.View
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import ru.hnau.androidutils.ui.view.utils.getMaxSquareMeasuredDimensionSize
import ru.hnau.androidutils.ui.view.utils.horizontalPaddingSum
import ru.hnau.androidutils.ui.view.utils.verticalPaddingSum
import ru.hnau.jutils.getter.Getter


@SuppressLint("ViewConstructor")
class QrCodeView(
        context: Context,
        initialCode: String? = null
) : View(context) {

    private val onMeasureResult = Point()

    private val drawable = QrCodeDrawable(
            context = context,
            initialCode = initialCode
    )

    var code: String?
        set(value) {
            drawable.code = value
            invalidate()
        }
        get() = drawable.code

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        drawable.draw(canvas)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        drawable.setBounds(paddingLeft, paddingTop, width - horizontalPaddingSum, height - verticalPaddingSum)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        getMaxSquareMeasuredDimensionSize(widthMeasureSpec, heightMeasureSpec, onMeasureResult)
        setMeasuredDimension(onMeasureResult.x, onMeasureResult.y)
    }

}