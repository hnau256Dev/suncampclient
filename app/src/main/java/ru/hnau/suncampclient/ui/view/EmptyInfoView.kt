package ru.hnau.suncampclient.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.androidutils.ui.view.utils.setTopPadding
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.sun_button.SunButton
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager


@SuppressLint("ViewConstructor")
class EmptyInfoView(
        context: Context,
        title: StringGetter,
        button: Pair<StringGetter, () -> Unit>? = null,
        titleColor: ColorGetter = ColorManager.BROWN,
        buttonColor: SunButtonColorInfo = SunButtonColorInfo.BROWN_ON_YELLOW
) : LinearLayout(
        context
) {

    companion object {

        fun createForLoadingError(
                context: Context,
                onNeedReload: (() -> Unit)? = null,
                titleColor: ColorGetter = ColorManager.BROWN,
                buttonColor: SunButtonColorInfo = SunButtonColorInfo.BROWN_ON_YELLOW
        ) =
                EmptyInfoView(
                        context = context,
                        title = StringGetter(R.string.empty_info_view_error_loading_title),
                        button = onNeedReload?.let { StringGetter(R.string.empty_info_view_error_loading_button) to it },
                        buttonColor = buttonColor,
                        titleColor = titleColor
                )

    }

    private val titleView = Label(
            context = context,
            initialText = title,
            info = LabelInfo(
                    fontType = FontManager.PT_SANS,
                    gravity = HGravity.CENTER,
                    textSize = DpPxGetter.fromDp(20),
                    textColor = titleColor
            )
    )

    private val buttonView = button?.let { (title, onClick) ->
        SunButton(
                context = context,
                size = SunButtonSizeInfo.SIZE_16,
                color = buttonColor,
                shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
                text = title,
                onClick = onClick
        ).apply {
            setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
            setTopPadding(SizeManager.DEFAULT_PADDING)
        }
    }

    init {
        orientation = VERTICAL
        gravity = Gravity.CENTER
        setPadding(SizeManager.DEFAULT_PADDING)

        addView(titleView)
        buttonView?.let(this::addView)
    }

}