package ru.hnau.suncampclient.ui.view.transactions_list

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.list.base.BaseListOrientation
import ru.hnau.androidutils.ui.view.list.group.GroupList
import ru.hnau.androidutils.ui.view.list.group.ListGroup
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncampclient.ui.view.GroupListTitleWrapper
import ru.hnau.suncampclient.utils.timestamp.TimestampType


@SuppressLint("ViewConstructor")
class TransactionsListView(
        context: Context,
        transactions: List<Transaction>,
        relativelyUserLogin: String
) : GroupList<StringGetter, Transaction>(
        context,
        itemsProducer = DataProducer(transactionsToGroups(transactions)),
        orientation = BaseListOrientation.VERTICAL,
        fixedSize = true,
        groupsViewWrappersCreator = { GroupListTitleWrapper(context) },
        itemsViewWrappersCreator = { TransactionViewWrapper(context, relativelyUserLogin) }
) {

    companion object {

        fun transactionsToGroups(transactions: List<Transaction>) =
                transactions
                        .groupBy { TimestampType.choose(it.timestamp) }
                        .toList()
                        .sortedBy { it.first.ordinal }
                        .map {
                            val title = it.first.title
                            val list = it.second.sortedByDescending { it.timestamp }
                            ListGroup(title, list)
                        }

    }

}