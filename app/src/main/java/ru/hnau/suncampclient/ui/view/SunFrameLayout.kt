package ru.hnau.suncampclient.ui.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawer
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawerPathShape
import ru.hnau.androidutils.ui.utils.shadow.info.ShadowInfo
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.initWithRoundSidesRect


open class SunFrameLayout(
        context: Context,
        shadowInfo: ShadowInfo = ColorManager.SHADOW_INFO_NORMAL,
        color: ColorGetter = ColorGetter.WHITE
) : FrameLayout(context) {

    private val contentRect = RectF()
    private val contentPath = Path()

    private val backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        this.color = color.get(context)
    }

    private val shadowDrawer = ShadowDrawer(
            context = context,
            shadowInfo = shadowInfo,
            shape = ShadowDrawerPathShape(contentPath)
    )

    init {
        setSoftwareRendering()
        setSuperPadding(0, 0, 0, 0)
    }

    override fun setPadding(left: Int, top: Int, right: Int, bottom: Int) =
            setSuperPadding(left, top, right, bottom)

    private fun setSuperPadding(left: Int, top: Int, right: Int, bottom: Int) = super.setPadding(
            left + shadowDrawer.shadowSizeRect.left.toInt(),
            top + shadowDrawer.shadowSizeRect.top.toInt(),
            right + shadowDrawer.shadowSizeRect.right.toInt(),
            bottom + shadowDrawer.shadowSizeRect.bottom.toInt()
    )

    override fun dispatchDraw(canvas: Canvas) {
        shadowDrawer.draw(canvas)
        canvas.drawPath(contentPath, backgroundPaint)
        super.dispatchDraw(canvas)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        contentRect.set(
                shadowDrawer.shadowSizeRect.left,
                shadowDrawer.shadowSizeRect.top,
                width - shadowDrawer.shadowSizeRect.right,
                height - shadowDrawer.shadowSizeRect.bottom
        )

        contentPath.initWithRoundSidesRect(contentRect)


    }

}