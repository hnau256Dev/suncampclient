package ru.hnau.suncampclient.ui.view

import android.content.Context
import android.view.View
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager


class GroupListTitleWrapper(
        context: Context,
        info: LabelInfo = DEFAULT_LABEL_INFO
) : Label(
        context = context,
        info = info,
        initialText = StringGetter.EMPTY
), BaseListViewWrapper<StringGetter> {

    companion object {

        private val DEFAULT_LABEL_INFO = LabelInfo(
                textColor = ColorManager.YELLOW,
                gravity = HGravity.LEFT_CENTER_VERTICAL,
                fontType = FontManager.PT_SANS,
                textSize = DpPxGetter.fromDp(24),
                minLines = 1,
                maxLines = 1
        )

    }

    init {
        setPadding(SizeManager.DEFAULT_PADDING, SizeManager.DEFAULT_PADDING, SizeManager.DEFAULT_PADDING, DpPxGetter.fromDp(8))
    }

    override val view: View
        get() = this

    override fun setContent(content: StringGetter) {
        text = content
    }

}