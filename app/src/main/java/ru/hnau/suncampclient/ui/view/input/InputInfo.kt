package ru.hnau.suncampclient.ui.view.input

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.DpPxGetter


data class InputInfo(
        val textSize: DpPxGetter = DpPxGetter.fromDp(24),
        val textColor: ColorGetter = ColorGetter.BLACK,
        val backgroundColor: ColorGetter = ColorGetter.WHITE,
        val maxLength: Int? = null,
        val imeAction: InputImeAction = InputImeAction.NEXT,
        val symbolsType: InputSymbolsType = InputSymbolsType.TEXT,
        val paddingHorizontal: DpPxGetter = DpPxGetter.fromDp(8),
        val paddingVertical: DpPxGetter = DpPxGetter.fromDp(8)
)