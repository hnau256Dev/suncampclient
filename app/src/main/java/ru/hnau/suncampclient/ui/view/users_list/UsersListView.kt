package ru.hnau.suncampclient.ui.view.users_list

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.list.base.BaseListOrientation
import ru.hnau.androidutils.ui.view.list.group.GroupList
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.ui.view.GroupListTitleWrapper
import ru.hnau.suncampclient.utils.splitToGroups


@SuppressLint("ViewConstructor")
class UsersListView(
        context: Context,
        users: List<User>,
        private val onItemClicked: (User) -> Unit
) : GroupList<StringGetter, User>(
        context = context,
        orientation = BaseListOrientation.VERTICAL,
        fixedSize = true,
        itemsProducer = DataProducer(users.splitToGroups()),
        groupsViewWrappersCreator = { GroupListTitleWrapper(context) },
        itemsViewWrappersCreator = { UsersListItemView(context, onItemClicked) }
)