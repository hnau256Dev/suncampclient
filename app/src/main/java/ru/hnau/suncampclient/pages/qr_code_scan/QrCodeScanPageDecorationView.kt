package ru.hnau.suncampclient.pages.qr_code_scan

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.inscribe_drawable_view.InscribeDrawableView
import ru.hnau.suncampclient.ui.view.inscribe_drawable_view.InscribeDrawableViewMaximizationType


class QrCodeScanPageDecorationView(
        context: Context
) : LinearLayout(
        context
) {

    companion object {

        private val SHADOW_COLOR = Color.argb(191, 0, 0, 0)
        private const val PADDING_HORIZONTAL_WEIGHT = 0.4f

    }

    private val leftView = View(context).apply {
        setLinearLayoutLayoutParams(0, MATCH_PARENT, PADDING_HORIZONTAL_WEIGHT)
        setBackgroundColor(SHADOW_COLOR)
    }

    private val topView = View(context).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f)
        setBackgroundColor(SHADOW_COLOR)
    }

    private val centerView = InscribeDrawableView(
            context = context,
            maximizationType = InscribeDrawableViewMaximizationType.HORIZONTAL,
            drawable = DrawableGetter(R.drawable.ic_qr_template)
    )

    private val bottomView = View(context).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f)
        setBackgroundColor(SHADOW_COLOR)
    }

    private val middleView = LinearLayout(context).apply {
        orientation = VERTICAL
        setLinearLayoutLayoutParams(0, MATCH_PARENT, 1f)

        addView(topView)
        addView(centerView)
        addView(bottomView)

    }

    private val rightView = View(context).apply {
        setLinearLayoutLayoutParams(0, MATCH_PARENT, PADDING_HORIZONTAL_WEIGHT)
        setBackgroundColor(SHADOW_COLOR)
    }

    init {
        orientation = HORIZONTAL

        addView(leftView)
        addView(middleView)
        addView(rightView)
    }

}