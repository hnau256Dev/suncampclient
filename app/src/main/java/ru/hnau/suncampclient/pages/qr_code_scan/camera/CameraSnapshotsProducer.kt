package ru.hnau.suncampclient.pages.qr_code_scan.camera

import ru.hnau.jutils.producer.Producer


class CameraSnapshotsProducer : Producer<CameraSnapshot>() {

    var producing = true


    fun onNewSnapshot(snapshot: CameraSnapshot) {
        if (!producing) {
            return
        }
        call(snapshot)
    }

}