package ru.hnau.suncampclient.pages.main.view.wallet.balance

import android.content.Context
import android.view.Gravity
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.view.buttons.circle.CircleButtonSize
import ru.hnau.androidutils.ui.view.buttons.circle.icon.CircleIconButton
import ru.hnau.androidutils.ui.view.buttons.circle.icon.CircleIconButtonInfo
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setFrameLayoutLayoutParams
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.income_manager.IncomeManager
import ru.hnau.suncampclient.utils.outcome_manager.OutcomeManager


class MainPageWalletBalanceInnerView(context: Context) : FrameLayout(context) {

    private val valueView = MainPageWalletBalanceValueView(context)

    private val incomeButton = CircleIconButton(
            context = context,
            info = CircleIconButtonInfo.DEFAULT,
            onClick = IncomeManager::doIncome,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
            size = CircleButtonSize.DEFAULT,
            rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
            icon = DrawableGetter(R.drawable.ic_income)
    ).apply {
        setPadding(DpPxGetter.fromDp(6))
        setFrameLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            gravity = Gravity.TOP or Gravity.RIGHT
        }
    }

    private val outcomeButton = CircleIconButton(
            context = context,
            info = CircleIconButtonInfo.DEFAULT,
            onClick = OutcomeManager::doOutcome,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
            size = CircleButtonSize.DEFAULT,
            rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
            icon = DrawableGetter(R.drawable.ic_outcome)
    ).apply {
        setPadding(DpPxGetter.fromDp(6))
        setFrameLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            gravity = Gravity.BOTTOM or Gravity.RIGHT
        }
    }

    init {
        addView(valueView)
        addView(outcomeButton)
        addView(incomeButton)
    }

    fun updateData() = valueView.updateData()


}