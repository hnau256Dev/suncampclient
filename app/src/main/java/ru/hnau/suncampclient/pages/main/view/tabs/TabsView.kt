package ru.hnau.suncampclient.pages.main.view.tabs

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams


open class TabsView(
        context: Context,
        private val tabs: List<Tab>
) : LinearLayout(context) {

    private val pager = ViewPager(context).apply {

        adapter = object : PagerAdapter() {

            override fun isViewFromObject(view: View, any: Any) = view == any

            override fun getCount() = tabs.size

            override fun instantiateItem(container: ViewGroup, position: Int) =
                    tabs[position].viewGenerator.invoke(context).apply(container::addView)

            override fun destroyItem(container: ViewGroup, position: Int, any: Any) {
                (any as? View)?.apply(container::removeView)
            }

        }

        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f)

    }

    private val buttonsView = TabsButtons(
            pager = pager,
            tabs = tabs
    ).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT)
    }

    init {
        orientation = VERTICAL
        addView(pager)
        addView(buttonsView)
    }

}