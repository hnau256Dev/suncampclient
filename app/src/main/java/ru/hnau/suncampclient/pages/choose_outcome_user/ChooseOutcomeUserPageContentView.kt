package ru.hnau.suncampclient.pages.choose_outcome_user

import android.content.Context
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.ContentLoader
import ru.hnau.jutils.finisher.awaitSuccess
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.pages.new_transaction.NewTransactionPage
import ru.hnau.suncampclient.pages.new_transaction.param.NewTransactionParam
import ru.hnau.suncampclient.pages.new_transaction.param.NewUserToUserTransactionParam
import ru.hnau.suncampclient.ui.view.EmptyInfoView
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.users_list.UsersListView
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.LoginManager


class ChooseOutcomeUserPageContentView(
        context: Context
) : ContentLoader<Possible<List<User>>>(
        context = context,
        fromSide = Side.BOTTOM,
        scrollFactor = 0.5f,
        contentProducer = UsersManager
) {

    override fun generateContentView(data: Possible<List<User>>) =
            data.handle(
                    onSuccess = { users ->
                        val otherUsers = users.filter { it.login != LoginManager.login }
                        UsersListView(context, otherUsers) {

                            UsersManager.getByLogin(LoginManager.login).awaitSuccess { me ->
                                AppActivityConnector.showPage(
                                        NewTransactionPage(
                                                NewUserToUserTransactionParam(
                                                        fromUser = me,
                                                        toUser = it
                                                )
                                        )
                                )
                            }


                        }
                    },
                    onError = {
                        EmptyInfoView.createForLoadingError(
                                context = context,
                                onNeedReload = UsersManager::invalidate,
                                titleColor = ColorManager.YELLOW,
                                buttonColor = SunButtonColorInfo.BROWN_ON_YELLOW
                        )
                    }
            )

    override fun generateWaiterView(lockedProducer: LockedProducer) =
            ColorManager.createWaiter(context, lockedProducer)

}