package ru.hnau.suncampclient.pages.qr_code_scan.camera

import android.hardware.Camera
import ru.hnau.jutils.tryOrNull


class CameraSnapshot(
        val bytes: ByteArray,
        val width: Int,
        val height: Int
) {

    companion object {

        fun fromRawData(data: ByteArray, camera: Camera) = tryOrNull {
            val previewSize = camera.parameters.previewSize
            CameraSnapshot(data, previewSize.width, previewSize.height)
        }

    }

}