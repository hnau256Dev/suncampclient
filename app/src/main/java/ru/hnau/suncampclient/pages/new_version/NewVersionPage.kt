package ru.hnau.suncampclient.pages.new_version

import android.content.Context
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.suncampclient.utils.AppActivityConnector


class NewVersionPage(
        private val info: NewVersionInfo
) : Page<NewVersionPageView>() {

    companion object {

        fun show(info: NewVersionInfo) {
            AppActivityConnector.showPage(NewVersionPage(info), clearStack = info.hard)
        }

    }

    override fun generateView(context: Context) = NewVersionPageView(context, info)

}