package ru.hnau.suncampclient.pages.qr_code_scan.camera

import android.Manifest
import android.hardware.Camera
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.PermissionsRequester
import ru.hnau.androidutils.utils.runUi
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.finisher.await
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.error
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.jutils.tryCatch
import ru.hnau.jutils.tryOrElse
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.PermissionExplanationShower
import kotlin.concurrent.thread


object CameraManager : DataProducer<Possible<Camera>>(Possible.undefined()) {

    private var listenersExists = false

    private var autoFocusManager: CameraAutoFocusManager? = null

    var torch = false
        set(value) {
            if (field != value) {
                field = value
                setCameraTorch(value)
            }
        }

    private fun setCameraTorch(torch: Boolean = CameraManager.torch, camera: Camera? = data.data) {
        camera ?: return
        tryCatch {
            val params = camera.parameters
            params.flashMode = if (torch) Camera.Parameters.FLASH_MODE_TORCH else Camera.Parameters.FLASH_MODE_OFF
            camera.parameters = params
        }
    }

    override fun onFirstAttached() {
        super.onFirstAttached()
        listenersExists = true

        synchronized(this) {
            release()
            Finisher(this::getCamera).await {
                runUi {
                    if (listenersExists) {
                        data = it
                        it.data?.let(this::onNewCamera)
                    }
                }
            }
        }

    }

    private fun onNewCamera(camera: Camera) {
        autoFocusManager = CameraAutoFocusManager(camera)
        setCameraTorch(camera = camera)
    }

    private fun getCamera(onFinished: (Possible<Camera>) -> Unit) {
        val activity = AppActivityConnector.appActivity
        if (activity == null) {
            onFinished.error()
            return
        }

        PermissionsRequester.requestPermission(
                activity = activity,
                onNeedShowExplanation = PermissionExplanationShower.getOnNeedShowExplanation(StringGetter(R.string.qr_code_scan_page_view_camera_permission_explanation)),
                permissionName = Manifest.permission.CAMERA
        )

                .await(
                        onSuccess = {
                            thread {
                                val camera = getCameraSync(3)
                                onFinished.invoke(Possible.trySuccess(camera))
                            }
                        },
                        onError = {
                            onFinished.error(it)
                        }
                )
    }

    private fun getCameraSync(attempts: Int): Camera? {
        if (attempts <= 0) {
            return null
        }
        return tryOrElse(
                throwsAction = Camera::open,
                onThrow = {
                    Thread.sleep(100)
                    getCameraSync(attempts - 1)
                }
        )
    }

    override fun onLastDetached() {
        super.onLastDetached()
        listenersExists = false
        release()
    }

    private fun release() = synchronized(this) {
        autoFocusManager?.release()
        autoFocusManager = null
        data.data?.release()
        data = Possible.undefined()
    }


}