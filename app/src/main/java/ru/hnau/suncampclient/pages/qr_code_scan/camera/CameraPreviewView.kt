package ru.hnau.suncampclient.pages.qr_code_scan.camera

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.Camera
import android.view.SurfaceHolder
import android.view.SurfaceView
import ru.hnau.jutils.tryCatch


@SuppressLint("ViewConstructor")
class CameraPreviewView(
        context: Context,
        private val camera: Camera
) : SurfaceView(context), SurfaceHolder.Callback {

    val snapshotsProducer = CameraSnapshotsProducer()

    private val surfaceHolder = this.holder.apply {
        addCallback(this@CameraPreviewView)
        setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
    }

    init {
        camera.setDisplayOrientation(90)
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        surfaceDestroyed(holder)
        surfaceCreated(holder)
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        tryCatch {
            camera.setPreviewCallback(null)
            camera.stopPreview()
            camera.setPreviewDisplay(null)
        }
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        tryCatch {
            camera.setPreviewDisplay(surfaceHolder)
            camera.startPreview()
            camera.setPreviewCallback(this::onSnapshot)
        }
    }

    private fun onSnapshot(data: ByteArray, camera: Camera) {
        val snapshot = CameraSnapshot.fromRawData(data, camera) ?: return
        snapshotsProducer.onNewSnapshot(snapshot)
    }

}