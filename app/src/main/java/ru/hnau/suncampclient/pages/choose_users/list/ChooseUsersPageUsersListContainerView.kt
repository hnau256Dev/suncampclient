package ru.hnau.suncampclient.pages.choose_users.list

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.ContentLoader
import ru.hnau.jutils.finisher.mapPossible
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.ui.view.EmptyInfoView
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.users_list.UsersListView
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class ChooseUsersPageUsersListContainerView(
        context: Context
) : ContentLoader<Possible<List<User>>>(
        context = context,
        scrollFactor = 0.25f,
        fromSide = Side.BOTTOM,
        contentProducer = UsersManager
) {

    private val selectedUsersContainer = SelectedUsersContainer()

    val selectedUsers: Set<User>
        get() = selectedUsersContainer.selectedUsers

    private var allUsers = emptyList<User>()

    override fun generateContentView(data: Possible<List<User>>) =
            data.handle(
                    onSuccess = {
                        allUsers = it
                        ChooseUsersPageUsersListView(
                                context = context,
                                users = it,
                                selectedUsersProducer = selectedUsersContainer,
                                onUserClicked = selectedUsersContainer::onUserClicked
                        )
                    },
                    onError = {
                        EmptyInfoView.createForLoadingError(
                                context = context,
                                onNeedReload = UsersManager::invalidate,
                                titleColor = ColorManager.YELLOW,
                                buttonColor = SunButtonColorInfo.BROWN_ON_YELLOW
                        )
                    }
            )

    fun selectAll() = selectedUsersContainer.selectAll(allUsers)

    fun unselectAll() = selectedUsersContainer.unselectAll()

    override fun generateWaiterView(lockedProducer: LockedProducer) =
            ColorManager.createWaiter(context, lockedProducer)

}