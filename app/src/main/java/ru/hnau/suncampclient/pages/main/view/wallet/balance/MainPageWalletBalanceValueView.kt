package ru.hnau.suncampclient.pages.main.view.wallet.balance

import android.content.Context
import android.graphics.ColorMatrix
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.ContentLoader
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.setLeftPadding
import ru.hnau.androidutils.ui.view.utils.setRightPadding
import ru.hnau.androidutils.ui.view.utils.setTopPadding
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.BalanceManager
import ru.hnau.suncampclient.ui.view.EmptyInfoView
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager


class MainPageWalletBalanceValueView(
        context: Context
) : ContentLoader<Possible<Long>>(
        context = context,
        contentProducer = BalanceManager,
        fromSide = Side.BOTTOM
) {

    private val balanceTitle = Label(
            context = context,
            textColor = ColorManager.BROWN,
            textSize = DpPxGetter.fromDp(24),
            initialText = StringGetter(R.string.main_page_balance_title),
            minLines = 1,
            maxLines = 1,
            fontType = FontManager.PT_SANS
    ).apply {
        setRightPadding(DpPxGetter.fromDp(8))
        setTopPadding(DpPxGetter.fromDp(3))
        setLeftPadding(SizeManager.DEFAULT_PADDING)
    }

    private val balanceText = Label(
            context = context,
            textColor = ColorManager.BROWN,
            textSize = DpPxGetter.fromDp(36),
            minLines = 1,
            maxLines = 1,
            fontType = FontManager.PT_SANS
    )

    private val balanceContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER_VERTICAL or Gravity.LEFT

        addView(balanceTitle)
        addView(balanceText)
    }

    private val errorWhileLoadingView: View by lazy {
        EmptyInfoView.createForLoadingError(
                context = context,
                onNeedReload = BalanceManager::invalidate,
                buttonColor = SunButtonColorInfo.BROWN_ON_ORANGE,
                titleColor = ColorManager.BROWN
        ).apply {
            setRightPadding(DpPxGetter.fromDp(112))
        }
    }

    override fun generateContentView(data: Possible<Long>) =
            data.handle(
                    onSuccess = this::generateSuccessView,
                    onError = this::generateErrorView
            )

    private fun generateSuccessView(balance: Long): LinearLayout {
        balanceText.text = balance.toString().toGetter()
        return balanceContainer
    }

    private fun generateErrorView(ex: Exception?) = errorWhileLoadingView

    override fun generateWaiterView(lockedProducer: LockedProducer) =
            ColorManager.createWaiter(context, lockedProducer)


    fun updateData() {
        BalanceManager.invalidate()
    }

}