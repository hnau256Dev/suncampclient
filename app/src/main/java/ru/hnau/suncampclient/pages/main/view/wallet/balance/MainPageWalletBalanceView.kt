package ru.hnau.suncampclient.pages.main.view.wallet.balance

import android.content.Context
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.dp_px.DpPx
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.inscribe_drawable_view.InscribeDrawableView
import ru.hnau.suncampclient.ui.view.inscribe_drawable_view.InscribeDrawableViewMaximizationType
import ru.hnau.suncampclient.utils.ColorManager


class MainPageWalletBalanceView(context: Context) : FrameLayout(context) {


    private val backgroundView = InscribeDrawableView(
            context = context,
            drawable = DrawableGetter(R.drawable.ic_card_background),
            maximizationType = InscribeDrawableViewMaximizationType.HORIZONTAL
    ).apply {
        setFrameLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT) {
            setRightMargin(DpPx.fromDp(context, 80))
        }
    }

    private val innerView = MainPageWalletBalanceInnerView(context).apply {
        setFrameLayoutLayoutParams(MATCH_PARENT, MATCH_PARENT)
    }


    init {
        setBackgroundColor(ColorManager.BROWN)
        addView(backgroundView)
        addView(innerView)
    }

    fun updateData() = innerView.updateData()


}