package ru.hnau.suncampclient.pages.login

import android.content.Context
import android.view.Gravity
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.androidutils.ui.view.utils.setMargins
import ru.hnau.androidutils.utils.ToastDuration
import ru.hnau.androidutils.utils.showToast
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.validators.LoginValidator
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.input.Input
import ru.hnau.suncampclient.ui.view.input.InputImeAction
import ru.hnau.suncampclient.ui.view.sun_button.SunButton
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.LoginManager
import ru.hnau.suncampclient.utils.SizeManager


class LoginPageView(
        context: Context,
        private val onNextClicked: (login: String) -> Unit
) : LinearLayout(
        context
) {

    private val defaultPadding = SizeManager.DEFAULT_PADDING.getPx(context)

    private val title = Label(
            context = context,
            initialText = StringGetter(R.string.login_page_login_title),
            info = ColorManager.LABEL_INFO_TITLE
    ).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT) {
            setMargins(defaultPadding, 0f, defaultPadding, defaultPadding)
        }
    }

    private val loginInput = Input(
            context = context,
            info = ColorManager.INPUT_INFO.copy(
                    imeAction = InputImeAction.DONE
            ),
            initialText = LoginManager.login,
            hintText = StringGetter(R.string.login_page_login_input_hint),
            shadowInfo = ColorManager.SHADOW_INFO_NORMAL
    ).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT) {
            setMargins(defaultPadding, 0f, defaultPadding, 0f)
        }
    }

    private val nextButton = SunButton(
            context = context,
            text = StringGetter(R.string.login_page_button_next),
            onClick = this::onNextButtonClicked,
            color = SunButtonColorInfo.WHITE_ON_ORANGE,
            size = SunButtonSizeInfo.SIZE_24,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO
    ).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT) {
            setMargins(defaultPadding, 0f, defaultPadding, defaultPadding)
        }
    }

    init {

        orientation = VERTICAL
        gravity = Gravity.CENTER

        addView(title)
        addView(loginInput)
        addView(nextButton)
    }

    private fun onNextButtonClicked() {
        val login = loginInput.text.toString()
        try {
            LoginValidator.validateLogin(login)
        } catch (ex: ApiErrorException) {
            showToast(StringGetter(ex.apiError.text), ToastDuration.LONG)
            return
        }
        onNextClicked.invoke(login)
    }


}