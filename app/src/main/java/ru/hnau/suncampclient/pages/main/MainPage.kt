package ru.hnau.suncampclient.pages.main

import android.content.Context
import android.view.View
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.suncampclient.pages.main.view.MainPageViewCreator


class MainPage : Page<View>() {

    override fun generateView(context: Context) =
            MainPageViewCreator.create(context)

}