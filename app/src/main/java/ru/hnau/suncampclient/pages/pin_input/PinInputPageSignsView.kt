package ru.hnau.suncampclient.pages.pin_input

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import ru.hnau.androidutils.animations.StatesAnimator
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.utils.drawing.ColorUtils
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawer
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawerShape
import ru.hnau.androidutils.ui.utils.shadow.info.ShadowInfo
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.horizontalPaddingSum
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering
import ru.hnau.androidutils.ui.view.utils.verticalPaddingSum
import ru.hnau.jutils.TimeValue
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class PinInputPageSignsView(
        context: Context,
        private val maxCount: Int
) : View(context) {

    companion object {

        private val INACTIVE_RADIUS = DpPxGetter.fromDp(8)
        private val ACTIVE_RADIUS = DpPxGetter.fromDp(10)
        private val SEPARATION = DpPxGetter.fromDp(32)
        private val INACTIVE_COLOR = ColorManager.WHITE
        private val ACTIVE_COLOR = ColorManager.ORANGE

    }

    private val inactiveRadius = INACTIVE_RADIUS.getPx(context)
    private val activeRadius = ACTIVE_RADIUS.getPx(context)
    private val separation = SEPARATION.getPx(context)
    private val inactiveColor = INACTIVE_COLOR.get(context)
    private val activeColor = ACTIVE_COLOR.get(context)

    private val statesAnimator = StatesAnimator(
            interStatesAnimatingTime = TimeValue.MILLISECOND * 100
    )

    private data class Circle(
            var cx: Float = 0f,
            var cy: Float = 0f,
            var activePercentage: Float = 0f
    )

    private val circles = (0 until maxCount).map { Circle() }

    private val shadowDrawer = ShadowDrawer(
            context = context,
            shadowInfo = ShadowInfo.DEFAULT,
            shape = ShadowDrawerShape { canvas, paint ->
                circles.forEach {
                    it.draw(canvas, paint, false)
                }
            }
    )

    private val circlesPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    var count = 0
        set(value) {
            if (field != value) {
                field = value
                statesAnimator.animateTo(value)
            }
        }

    private var realCount = 0f
        set(value) {
            field = value
            updateCirclesActivePercentages(value)
            invalidate()
        }

    init {
        setSoftwareRendering()
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        shadowDrawer.draw(canvas)
        circles.forEach {
            it.draw(canvas, circlesPaint, true)
        }
    }

    private fun Circle.draw(canvas: Canvas, paint: Paint, useColor: Boolean) {
        if (useColor) {
            paint.color = ColorUtils.colorInterColors(inactiveColor, activeColor, activePercentage)
        }
        val radius = inactiveRadius + (activeRadius - inactiveRadius) * activePercentage
        canvas.drawCircle(cx, cy, radius, paint)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        val cx = paddingLeft + shadowDrawer.shadowSizeRect.left + (width - horizontalPaddingSum - shadowDrawer.shadowSizeRect.left - shadowDrawer.shadowSizeRect.right) / 2
        val cy = paddingTop + shadowDrawer.shadowSizeRect.top + (height - verticalPaddingSum - shadowDrawer.shadowSizeRect.top - shadowDrawer.shadowSizeRect.bottom) / 2

        val firstCircleCx = cx - (maxCount / 2f - 0.5f) * separation

        circles.forEachIndexed { i, circle ->
            circle.cx = firstCircleCx + i * separation
            circle.cy = cy
        }

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val circlesWidth = activeRadius + separation * maxCount
        val width = circlesWidth + shadowDrawer.shadowSizeRect.left + shadowDrawer.shadowSizeRect.right + horizontalPaddingSum
        val height = activeRadius + shadowDrawer.shadowSizeRect.top + shadowDrawer.shadowSizeRect.bottom + verticalPaddingSum
        setMeasuredDimension(
                getDefaultMeasurement(widthMeasureSpec, width.toInt()),
                getDefaultMeasurement(heightMeasureSpec, height.toInt())
        )
    }

    private fun updateCirclesActivePercentages(realCount: Float) {
        circles.forEachIndexed { i, circle ->
            circle.activePercentage = (realCount - i).coerceIn(0f, 1f)
        }
    }

    private fun onRealCountChanged(realCount: Float) {
        this.realCount = realCount
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        statesAnimator.attach(this::onRealCountChanged)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        statesAnimator.detach(this::onRealCountChanged)
    }

}