package ru.hnau.suncampclient.pages.pin_input

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager


@SuppressLint("ViewConstructor")
class PinInputPageView(
        context: Context,
        private val signsCount: Int,
        title: StringGetter,
        private val afterEnter: (String, PinInputPageConnector) -> Unit,
        additionalViewBuilder: ((Context, PinInputPageConnector) -> View)?,
        private val showPage: (page: Page<*>, clearStack: Boolean, precedingPages: List<Page<*>>) -> Boolean,
        private val goBack: (pageResolver: (List<Page<*>>) -> Int?) -> Boolean
) : FrameLayout(
        context
), PinInputPageConnector {

    private val lockedProducer = FinishersLockedProducer()

    private val waiterView = ColorManager.createWaiter(context, lockedProducer)

    override val finishersLockedProducer: FinishersLockedProducer
        get() = lockedProducer

    private var pinText = ""
        set(value) {
            if (field != value) {
                val normalizedValue = value.take(signsCount)
                field = normalizedValue
                onPinChanged(normalizedValue)
            }
        }

    private val defaultPadding = SizeManager.DEFAULT_PADDING.getPxInt(context)

    private val titleView = Label(
            context = context,
            textColor = ColorManager.YELLOW,
            textSize = DpPxGetter.fromDp(28f),
            maxLines = 1,
            minLines = 1,
            initialText = title,
            gravity = HGravity.CENTER,
            fontType = FontManager.PT_SANS
    ).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f) {
            setMargins(defaultPadding, defaultPadding, defaultPadding, 0)
        }
    }

    private val signsView = PinInputPageSignsView(
            context = context,
            maxCount = signsCount
    ).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 0.5f) {
            setMargins(defaultPadding, defaultPadding, defaultPadding, 0)
        }
    }

    private val keyboard = PinInputKeyboard(context, this::onNumberClicked, this::onBackspaceClicked).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 5f) {
            setMargins(defaultPadding, defaultPadding, defaultPadding, 0)
        }
    }

    private val additionalView =
            (additionalViewBuilder?.invoke(context, this) ?: View(context)).apply {
                setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f) {
                    setMargins(defaultPadding, defaultPadding, defaultPadding, defaultPadding)
                }
            }

    private val contentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.VERTICAL
        addView(titleView)
        addView(signsView)
        addView(keyboard)
        addView(additionalView)
    }

    init {
        addView(contentContainer)
        addView(waiterView)
    }

    override fun showPage(page: Page<*>, clearStack: Boolean, precedingPages: List<Page<*>>) =
            showPage.invoke(page, clearStack, precedingPages)

    override fun goBack(pageResolver: (List<Page<*>>) -> Int?) =
            goBack.invoke(pageResolver)

    private fun onPinChanged(pin: String) {
        signsView.count = pin.length
        if (pin.length == signsCount) {
            afterEnter.invoke(pin, this)
        }
    }

    private fun onNumberClicked(number: Int) {
        pinText += number.toString()
    }

    private fun onBackspaceClicked() {
        pinText = pinText.dropLast(1)
    }

    override fun clearInput() {
        pinText = ""
    }

    override fun setPin(pin: String) {
        pinText = pin
    }

    override fun getPin() = pinText

}