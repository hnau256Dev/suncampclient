package ru.hnau.suncampclient.pages.main.view.storages

import android.content.Context
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.AutoSwipeRefreshView
import ru.hnau.androidutils.ui.view.ContentLoader
import ru.hnau.androidutils.utils.showToast
import ru.hnau.jutils.finisher.await
import ru.hnau.jutils.finisher.mapPossibleOrError
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.error
import ru.hnau.jutils.possible.success
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.suncamp.common.api.response.SubordinateStorageInfo
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.SubordinateStoragesManager
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.pages.choose_users.ChooseUsersPage
import ru.hnau.suncampclient.pages.choose_users.ChooseUsersPageConnector
import ru.hnau.suncampclient.pages.new_transaction.NewTransactionPage
import ru.hnau.suncampclient.pages.new_transaction.param.NewStorageToUsersTransactionParam
import ru.hnau.suncampclient.pages.new_transaction.param.NewUsersToStorageTransactionParam
import ru.hnau.suncampclient.pages.transactions_list.TransactionsListPage
import ru.hnau.suncampclient.ui.view.EmptyInfoView
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.users_list.UsersListView
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager
import ru.hnau.suncampclient.utils.dialog_manager.list.DialogManagerListViewItem


class StoragesView(
        context: Context
) : AutoSwipeRefreshView(
        context = context,
        color = ColorManager.RED
) {

    private val contentView = object : ContentLoader<Possible<List<Pair<SubordinateStorageInfo, User>>>>(
            context = context,
            fromSide = Side.BOTTOM,
            scrollFactor = 0.25f,
            contentProducer = SubordinateStoragesManager.map {
                it.mapPossibleOrError<List<SubordinateStorageInfo>, List<Pair<SubordinateStorageInfo, User>>> { storages, callback ->
                    UsersManager.currentData.await(
                            onError = {
                                callback.error(it)
                            },
                            onSuccess = { users ->
                                val result = storages.map { storage ->
                                    val user = users.find { it.login == storage.storageUserLogin }
                                    if (user == null) {
                                        callback.error()
                                        return@await
                                    }
                                    storage to user
                                }
                                callback.success(result)
                            }
                    )
                }
            }
    ) {

        override fun generateContentView(data: Possible<List<Pair<SubordinateStorageInfo, User>>>) =
                data.handle(
                        onSuccess = {
                            if (it.isEmpty()) {
                                EmptyInfoView(
                                        context = context,
                                        title = StringGetter(R.string.main_page_storages_no_storages),
                                        titleColor = ColorManager.YELLOW
                                )
                            } else {
                                SubordinateStoragesListView(context, it, this@StoragesView::onStorageClicked)
                            }
                        },
                        onError = {
                            EmptyInfoView.createForLoadingError(
                                    context = context,
                                    onNeedReload = SubordinateStoragesManager::invalidate,
                                    titleColor = ColorManager.YELLOW,
                                    buttonColor = SunButtonColorInfo.BROWN_ON_YELLOW
                            )
                        }
                )

        override fun generateWaiterView(lockedProducer: LockedProducer) =
                ColorManager.createWaiter(context, lockedProducer)

    }

    init {
        addView(contentView)
    }

    override fun updateContent() {
        SubordinateStoragesManager.invalidate()
    }

    private fun onStorageClicked(storage: Pair<SubordinateStorageInfo, User>) {
        DialogManager.show<Unit> {

            list(
                    listOf(
                            DialogManagerListViewItem(
                                    icon = DrawableGetter(R.drawable.ic_transactions),
                                    title = StringGetter(R.string.main_page_storages_transfer_dialog_title_transactions),
                                    subtitle = StringGetter(R.string.main_page_storages_transfer_dialog_subtitle_transactions),
                                    action = { AppActivityConnector.showPage(
                                            TransactionsListPage(
                                                    title = StringGetter(R.string.main_page_storages_storage_transactions),
                                                    transactions = storage.first.transactions,
                                                    relativelyUserLogin = storage.second.login
                                            )
                                    ) }
                            ),
                            DialogManagerListViewItem(
                                    icon = DrawableGetter(R.drawable.ic_income_orange),
                                    title = StringGetter(R.string.main_page_storages_transfer_dialog_title_income),
                                    subtitle = StringGetter(R.string.main_page_storages_transfer_dialog_subtitle_income),
                                    action = { income(storage.second) }
                            ),
                            DialogManagerListViewItem(
                                    icon = DrawableGetter(R.drawable.ic_outcome_orange),
                                    title = StringGetter(R.string.main_page_storages_transfer_dialog_title_outcome),
                                    subtitle = StringGetter(R.string.main_page_storages_transfer_dialog_subtitle_outcome),
                                    action = { outcome(storage.second) }
                            )
                    )
            )

        }
    }

    private fun selectUsers(
            storage: User,
            title: StringGetter,
            after: (storage: User, toUsers: Set<User>, connector: ChooseUsersPageConnector) -> Unit
    ) {
        AppActivityConnector.showPage(
                ChooseUsersPage(
                        title = title,
                        onUsersWasChosen = { users, connector ->
                            if (users.isEmpty()) {
                                showToast(StringGetter(R.string.main_page_storages_no_selected_users_error))
                                return@ChooseUsersPage
                            }
                            after.invoke(storage, users, connector)
                        }
                )
        )
    }

    private fun income(storage: User) =
            selectUsers(
                    storage = storage,
                    title = StringGetter(R.string.main_page_storages_income_choose_users_title),
                    after = this::incomeFromUsers
            )

    private fun outcome(storage: User) =
            selectUsers(
                    storage = storage,
                    title = StringGetter(R.string.main_page_storages_outcome_choose_users_title),
                    after = this::outcomeToUsers
            )

    private fun incomeFromUsers(storage: User, fromUsers: Set<User>, connector: ChooseUsersPageConnector) {
        val param = NewUsersToStorageTransactionParam(
                fromUsers = fromUsers,
                toUser = storage
        )
        connector.showPage(NewTransactionPage(param))
    }

    private fun outcomeToUsers(storage: User, toUsers: Set<User>, connector: ChooseUsersPageConnector) {
        val param = NewStorageToUsersTransactionParam(
                fromUser = storage,
                toUsers = toUsers
        )
        connector.showPage(NewTransactionPage(param))
    }

}