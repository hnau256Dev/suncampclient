package ru.hnau.suncampclient.pages.cards

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.header.PageViewWithHeader
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.utils.ColorManager


class CardsPageView(
        context: Context,
        onBackClicked: () -> Unit
) : PageViewWithHeader<CardsPageInnerView>(
        context = context,
        title = StringGetter(R.string.card_page_title),
        rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
        onBackClicked = onBackClicked,
        contentView = CardsPageInnerView(context),
        headerInfo = ColorManager.HEADER_INFO,
        headerBackButtonInfo = ColorManager.HEADER_BACK_BUTTON_INFO,
        headerLabelInfo = ColorManager.HEADER_LABEL_INFO
)