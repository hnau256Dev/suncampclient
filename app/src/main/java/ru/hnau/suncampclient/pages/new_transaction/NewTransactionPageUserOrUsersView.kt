package ru.hnau.suncampclient.pages.new_transaction

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ScrollView
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setLeftMargin
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.SunClickableFrameLayout
import ru.hnau.suncampclient.ui.view.SunFrameLayout
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.*
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager


@SuppressLint("ViewConstructor")
class NewTransactionPageUserOrUsersView(
        context: Context,
        private val users: Set<User>
) : SunClickableFrameLayout(
        context = context,
        shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
        rippleDrawInfo = ColorManager.WHITE_RIPPLE_DRAW_INFO
) {

    constructor(
            context: Context,
            user: User
    ) : this(
            context = context,
            users = setOf(user)
    )

    companion object {

        fun iconAndTitleForUsers(size: Int): Pair<DrawableGetter, StringGetter> {
            val icon = DrawableGetter(R.drawable.ic_users)
            val title = size.toString().toGetter() + " " + StringGetter(
                    CountSuffixManager.getSuffix(
                            count = size,
                            forCount1 = R.string.new_transaction_page_users_1,
                            forCount2_4 = R.string.new_transaction_page_users_2,
                            forOther = R.string.new_transaction_page_users_3
                    )
            )
            return icon to title
        }

        fun iconAndTitleForUser(user: User): Pair<DrawableGetter, StringGetter> {
            val icon = DrawableGetter(if (user.type == UserType.storage) R.drawable.ic_storage else R.drawable.ic_user)
            val title = if (user.login == LoginManager.login) StringGetter(R.string.new_transaction_page_user_name_i) else StringGetter(user.name)
            return icon to title
        }

    }

    private val iconAndTitle = run {
        val usersCount = users.size
        if (usersCount == 1) {
            iconAndTitleForUser(users.first())
        } else {
            iconAndTitleForUsers(usersCount)
        }
    }

    private val iconView = ImageView(context).apply {
        setImageDrawable(iconAndTitle.first.get(context))
        setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
    }

    private val nameView = Label(
            context = context,
            initialText = iconAndTitle.second,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(20),
            gravity = HGravity.LEFT_CENTER_VERTICAL,
            fontType = FontManager.PT_SANS,
            textColor = ColorManager.BROWN
    ).apply {
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f) {
            setLeftMargin(SizeManager.DEFAULT_PADDING.get(context))
        }
    }

    private val userInfoContentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER
        setPadding(SizeManager.DEFAULT_PADDING)
        addView(iconView)
        addView(nameView)
    }


    init {
        addView(userInfoContentContainer)
    }

    override fun onClicked() {
        super.onClicked()
        DialogManager.show<Unit> {
            view(
                    ScrollView(context).apply {
                        addView(
                                Label(
                                        context = context,
                                        fontType = FontManager.PT_SANS,
                                        textColor = ColorManager.BROWN,
                                        textSize = DpPxGetter.fromDp(16),
                                        initialText = users.joinToString(
                                                separator = "\n",
                                                transform = { it.name }
                                        ).toGetter()
                                ).apply {
                                    setPadding(SizeManager.DEFAULT_PADDING)
                                }
                        )
                    }
            )
        }
    }

}