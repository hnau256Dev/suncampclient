package ru.hnau.suncampclient.pages.new_version


data class NewVersionInfo(
        val hard: Boolean,
        val description: String,
        val updateUrl: String? = null
)