package ru.hnau.suncampclient.pages.auth

import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.jutils.finisher.await
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.api.Api
import ru.hnau.suncampclient.pages.main.MainPage
import ru.hnau.suncampclient.pages.pin_input.PinInputPage
import ru.hnau.suncampclient.pages.pin_input.PinInputPageConnector
import ru.hnau.suncampclient.utils.LoginManager
import ru.hnau.suncampclient.utils.Utils


class AuthPage(
        login: String,
        private val info: AuthPageInfo
) : PinInputPage(
        signsCount = Utils.PIN_LENGTH,
        title = StringGetter(R.string.auth_page_title),
        afterInput = { pin, connector -> afterInput(login, pin, connector, info) },
        additionalViewBuilder = if (info.showChangeUserButton) ::AuthPageAdditionalView else null
) {

    override fun handleGoBack() = info.blockGoBack

    companion object {

        private fun afterInput(login: String, pin: String, pinInputPageConnector: PinInputPageConnector, info: AuthPageInfo) {
            Api.userLogin(
                    login = login,
                    password = pin
            )
                    .addToLocker(pinInputPageConnector.finishersLockedProducer)
                    .await(
                            onSuccess = {
                                LoginManager.onLogged(it)
                                afterLogin(pinInputPageConnector, info)
                            },
                            onError = {
                                pinInputPageConnector.clearInput()
                            }
                    )
        }

        private fun afterLogin(pinInputPageConnector: PinInputPageConnector, info: AuthPageInfo) {
            if (info.goBackAfterAuth) {
                pinInputPageConnector.goBack()
                return
            }

            pinInputPageConnector.showPage(MainPage(), true)
        }


    }

}