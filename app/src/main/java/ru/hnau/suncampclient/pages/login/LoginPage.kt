package ru.hnau.suncampclient.pages.login

import android.content.Context
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.suncampclient.utils.PreferencesManager
import ru.hnau.suncampclient.pages.auth.AuthPage
import ru.hnau.suncampclient.pages.auth.AuthPageInfo


class LoginPage : Page<LoginPageView>() {

    override fun generateView(context: Context) =
            LoginPageView(context, this::onNextClicked)

    private fun onNextClicked(login: String) {
        PreferencesManager.userLogin = login
        showPage(AuthPage(login, AuthPageInfo.FOR_FIRST_AFTER_LOGIN_INPUT))
    }

}