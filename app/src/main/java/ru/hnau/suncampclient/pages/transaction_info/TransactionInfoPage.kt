package ru.hnau.suncampclient.pages.transaction_info

import android.content.Context
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.suncamp.common.data.Transaction


class TransactionInfoPage(
        private val transaction: Transaction
) : Page<TransactionInfoPageView>() {

    override fun generateView(context: Context) =
            TransactionInfoPageView(context, this::onBackClicked, transaction)

    private fun onBackClicked() {
        goBack()
    }
}