package ru.hnau.suncampclient.pages.main.view

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.pages.main.view.storages.StoragesView
import ru.hnau.suncampclient.pages.main.view.tabs.Tab
import ru.hnau.suncampclient.pages.main.view.tabs.TabsView
import ru.hnau.suncampclient.pages.main.view.wallet.MainPageWalletView
import ru.hnau.suncampclient.utils.LoginManager


object MainPageViewCreator {

    fun create(context: Context): View {
        val userType = LoginManager.user?.type ?: UserType.user
        if (!userType.allowManageStoragesAccounts) {
            return MainPageWalletView(context)
        }

        return TabsView(
                context = context,
                tabs = listOf(
                        Tab(
                                icon = DrawableGetter(R.drawable.ic_tab_wallet),
                                title = StringGetter(R.string.main_page_tab_title_wallet),
                                viewGenerator = ::MainPageWalletView
                        ),
                        Tab(
                                icon = DrawableGetter(R.drawable.ic_tab_storage),
                                title = StringGetter(R.string.main_page_tab_title_storages),
                                viewGenerator = ::StoragesView
                        )
                )
        )

    }

}