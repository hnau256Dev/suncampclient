package ru.hnau.suncampclient.pages.new_transaction.param

import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.finisher.mapPossible
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncamp.common.data.TransactionCategory
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.api.Api
import ru.hnau.suncampclient.pages.new_transaction.TransactionOrNull


class NewUsersToStorageTransactionParam(
        val fromUsers: Set<User>,
        val toUser: User,
        amount: Long? = null,
        category: TransactionCategory = TransactionCategory.fine,
        comment: String = ""
) : NewTransactionParam(
        amount,
        category,
        comment
) {

    override fun execute(amount: Long, category: TransactionCategory, comment: String) =
            Api.transactionFromUsersToStorage(
                    fromUsers = fromUsers,
                    comment = comment,
                    amount = amount,
                    category = category,
                    toUserLogin = toUser.login
            ).mapPossible { data -> TransactionOrNull() }

}