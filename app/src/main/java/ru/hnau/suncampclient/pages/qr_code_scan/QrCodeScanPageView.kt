package ru.hnau.suncampclient.pages.qr_code_scan

import android.content.Context
import android.view.View
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawableLayoutType
import ru.hnau.androidutils.ui.view.header.PageViewWithHeader
import ru.hnau.androidutils.ui.view.header.button.HeaderIconButton
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.pages.qr_code_scan.camera.CameraManager
import ru.hnau.suncampclient.utils.ColorManager


class QrCodeScanPageView(
        context: Context,
        afterScan: (String, QrCodeScanPageConnector) -> Unit,
        decorationViewBuilder: ((Context, QrCodeScanPageConnector) -> View)? = null,
        showPage: (page: Page<*>, clearStack: Boolean, precedingPages: List<Page<*>>) -> Boolean,
        goBack: (pageResolver: (List<Page<*>>) -> Int?) -> Boolean,
        onBackClicked: () -> Unit
) : PageViewWithHeader<QrCodeScanPageInnerView>(
        context = context,
        title = StringGetter(R.string.qr_code_scan_page_title),
        rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
        onBackClicked = onBackClicked,
        contentView = QrCodeScanPageInnerView(context, afterScan, decorationViewBuilder, showPage, goBack),
        headerInfo = ColorManager.HEADER_INFO,
        headerBackButtonInfo = ColorManager.HEADER_BACK_BUTTON_INFO,
        headerLabelInfo = ColorManager.HEADER_LABEL_INFO
) {

    init {
        header.addView(HeaderIconButton(
                context = context,
                onClick = { CameraManager.torch = !CameraManager.torch },
                rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
                icon = LayoutDrawable(
                        context = context,
                        layoutType = LayoutDrawableLayoutType.WRAP_CONTENT,
                        content = DrawableGetter(R.drawable.ic_flash)
                ).toGetter()

        ))
    }

}