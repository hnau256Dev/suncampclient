package ru.hnau.suncampclient.pages.new_transaction

import android.content.Context
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.suncampclient.pages.new_transaction.param.NewTransactionParam


class NewTransactionPage(
        private val param: NewTransactionParam
) : Page<NewTransactionPageView>() {

    override fun generateView(context: Context) =
            NewTransactionPageView(context, param, this::onBackClicked)

    private fun onBackClicked() {
        goBack()
    }

}