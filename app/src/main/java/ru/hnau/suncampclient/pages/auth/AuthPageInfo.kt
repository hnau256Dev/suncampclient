package ru.hnau.suncampclient.pages.auth


data class AuthPageInfo(
        val showChangeUserButton: Boolean,
        val goBackAfterAuth: Boolean,
        val blockGoBack: Boolean
) {

    companion object {

        val FOR_NEED_LOGIN_ERROR = AuthPageInfo(
                showChangeUserButton = true,
                goBackAfterAuth = true,
                blockGoBack = true
        )

        val FOR_START = AuthPageInfo(
                showChangeUserButton = true,
                goBackAfterAuth = false,
                blockGoBack = false
        )

        val FOR_FIRST_AFTER_LOGIN_INPUT = AuthPageInfo(
                showChangeUserButton = false,
                goBackAfterAuth = false,
                blockGoBack = false
        )

    }

}