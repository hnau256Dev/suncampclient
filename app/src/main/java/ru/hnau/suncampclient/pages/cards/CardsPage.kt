package ru.hnau.suncampclient.pages.cards

import android.content.Context
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.jutils.finisher.await
import ru.hnau.jutils.finisher.awaitSuccess
import ru.hnau.suncampclient.api.Api
import ru.hnau.suncampclient.data.MyCardsManager
import ru.hnau.suncampclient.pages.qr_code_scan.QrCodeScanPage
import ru.hnau.suncampclient.utils.AppActivityConnector


class CardsPage : Page<CardsPageView>() {

    companion object {

        fun bindCard() {

            AppActivityConnector.showPage(
                    QrCodeScanPage(
                            afterScan = { cardUuid, qrCodeScanPageConnector ->

                                qrCodeScanPageConnector.scanning = false

                                Api.cardRegister(cardUuid)
                                        .addToLocker(qrCodeScanPageConnector.finishersLockedProducer)
                                        .await {
                                            qrCodeScanPageConnector.scanning = true
                                            it.ifSuccess {
                                                MyCardsManager.invalidate()
                                                qrCodeScanPageConnector.goBack()
                                            }
                                        }
                            }
                    )
            )

        }

    }

    override fun generateView(context: Context) =
            CardsPageView(
                    context = context,
                    onBackClicked = this::onBackClicked
            )

    private fun onBackClicked() {
        goBack()
    }

}