package ru.hnau.suncampclient.pages.main.view.wallet

import android.content.Context
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawableLayoutType
import ru.hnau.androidutils.ui.view.AutoSwipeRefreshView
import ru.hnau.androidutils.ui.view.buttons.circle.CircleButtonSize
import ru.hnau.androidutils.ui.view.buttons.circle.icon.CircleIconButton
import ru.hnau.androidutils.ui.view.buttons.circle.icon.CircleIconButtonInfo
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.WrapProducer
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.MyTransactionsManager
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.pages.cards.CardsPage
import ru.hnau.suncampclient.pages.login.LoginPage
import ru.hnau.suncampclient.pages.main.view.wallet.balance.MainPageWalletBalanceView
import ru.hnau.suncampclient.ui.view.transactions_list.TransactionsView
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.LoginManager
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager
import ru.hnau.suncampclient.utils.dialog_manager.list.DialogManagerListViewItem


class MainPageWalletView(context: Context) : AutoSwipeRefreshView(
        context = context,
        color = ColorManager.RED
) {

    companion object {

        private val TRANSACTIONS_PRODUCER = object : WrapProducer<Finisher<Possible<List<Transaction>>>, Finisher<Possible<List<Transaction>>>>(MyTransactionsManager) {

            override fun onWrappedProducerCall(data: Finisher<Possible<List<Transaction>>>) {
                call(
                        (data + UsersManager.currentData)
                                .map { (transactions, _) -> transactions }
                )
            }

        }

    }

    private val balanceView = MainPageWalletBalanceView(context).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT)
    }

    private val transactionsView = TransactionsView(context, TRANSACTIONS_PRODUCER, LoginManager.login).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 2f)
    }

    private val backgroundContainer = LinearLayout(context).apply {
        orientation = LinearLayout.VERTICAL
        addView(balanceView)
        addView(transactionsView)
    }

    private val optionsButton = CircleIconButton(
            context = context,
            size = CircleButtonSize.DP_56,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
            rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
            onClick = this::onOptionsButtonClick,
            info = CircleIconButtonInfo.DEFAULT,
            icon = LayoutDrawable(
                    context = context,
                    content = DrawableGetter(R.drawable.ic_menu_brown),
                    layoutType = LayoutDrawableLayoutType.WRAP_CONTENT
            ).toGetter()
    ).apply {
        setPadding(DpPxGetter.fromDp(6))
        setFrameLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            gravity = Gravity.RIGHT or Gravity.BOTTOM
        }
    }

    private val contentContainer = FrameLayout(context).apply {
        addView(backgroundContainer)
        addView(optionsButton)
    }

    init {
        addView(contentContainer)
    }

    override fun updateContent() {
        balanceView.updateData()
        transactionsView.updateData()
    }

    private fun onOptionsButtonClick() {
        DialogManager.show<Unit> {
            list(
                    listOf(
                            DialogManagerListViewItem(
                                    icon = DrawableGetter(R.drawable.ic_card),
                                    title = StringGetter(R.string.main_page_options_title_cards),
                                    subtitle = StringGetter(R.string.main_page_options_subtitle_cards),
                                    action = { AppActivityConnector.showPage(CardsPage()) }
                            ),
                            DialogManagerListViewItem(
                                    icon = DrawableGetter(R.drawable.ic_users),
                                    title = StringGetter(R.string.main_page_options_title_change_user),
                                    subtitle = StringGetter(R.string.main_page_options_subtitle_change_user),
                                    action = { AppActivityConnector.showPage(LoginPage(), true) }
                            )
                    )
            )

        }

    }

}