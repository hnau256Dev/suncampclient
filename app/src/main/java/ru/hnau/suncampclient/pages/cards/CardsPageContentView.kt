package ru.hnau.suncampclient.pages.cards

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.ContentLoader
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.suncamp.common.data.Card
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.MyCardsManager
import ru.hnau.suncampclient.pages.cards.CardsPage.Companion.bindCard
import ru.hnau.suncampclient.ui.view.EmptyInfoView
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class CardsPageContentView(
        context: Context,
        private val finishersLockedProducer: FinishersLockedProducer
) : ContentLoader<Possible<List<Card>>>(
        context = context,
        fromSide = Side.BOTTOM,
        scrollFactor = 0.25f,
        contentProducer = MyCardsManager
) {

    override fun generateContentView(data: Possible<List<Card>>) =
            data.handle<View>(
                    onError = {
                        EmptyInfoView.createForLoadingError(
                                context = context,
                                onNeedReload = MyCardsManager::invalidate,
                                titleColor = ColorManager.YELLOW,
                                buttonColor = SunButtonColorInfo.BROWN_ON_YELLOW
                        )
                    },
                    onSuccess = { cards ->
                        if (cards.isEmpty()) {
                            EmptyInfoView(
                                    context = context,
                                    title = StringGetter(R.string.card_page_empty_info_title),
                                    button = StringGetter(R.string.card_page_empty_info_button) to CardsPage.Companion::bindCard,
                                    titleColor = ColorManager.YELLOW,
                                    buttonColor = SunButtonColorInfo.BROWN_ON_YELLOW
                            )
                        } else {
                            CardsPageListView(
                                    context = context,
                                    cards = cards,
                                    finishersLockedProducer = finishersLockedProducer
                            )
                        }
                    }
            )

    override fun generateWaiterView(lockedProducer: LockedProducer) =
            ColorManager.createWaiter(context, lockedProducer)

}