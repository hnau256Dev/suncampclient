package ru.hnau.suncampclient.pages.transactions_list

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.header.PageViewWithHeader
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.pages.choose_outcome_user.ChooseOutcomeUserPageInnerView
import ru.hnau.suncampclient.ui.view.transactions_list.TransactionsView
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class TransactionsListPageView(
        context: Context,
        onBackClicked: () -> Unit,
        title: StringGetter,
        transactions: List<Transaction>,
        relativelyUserLogin: String
) : PageViewWithHeader<TransactionsView>(
        context = context,
        title = title,
        rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
        onBackClicked = onBackClicked,
        contentView = TransactionsView(context, DataProducer(Finisher.forExistenceData(Possible.success(transactions))), relativelyUserLogin),
        headerInfo = ColorManager.HEADER_INFO,
        headerBackButtonInfo = ColorManager.HEADER_BACK_BUTTON_INFO,
        headerLabelInfo = ColorManager.HEADER_LABEL_INFO
)