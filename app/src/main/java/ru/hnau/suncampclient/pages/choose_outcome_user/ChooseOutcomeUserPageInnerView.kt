package ru.hnau.suncampclient.pages.choose_outcome_user

import android.content.Context
import ru.hnau.androidutils.ui.view.AutoSwipeRefreshView
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.utils.ColorManager


class ChooseOutcomeUserPageInnerView(
        context: Context
) : AutoSwipeRefreshView(
        context = context,
        color = ColorManager.RED
) {

    init {
        addView(ChooseOutcomeUserPageContentView(context))
    }

    override fun updateContent() = UsersManager.invalidate()

}