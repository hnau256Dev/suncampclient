package ru.hnau.suncampclient.pages.choose_users.list

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.list.base.BaseListOrientation
import ru.hnau.androidutils.ui.view.list.group.GroupList
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.jutils.producer.Producer
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.ui.view.GroupListTitleWrapper
import ru.hnau.suncampclient.utils.splitToGroups


@SuppressLint("ViewConstructor")
class ChooseUsersPageUsersListView(
        context: Context,
        users: List<User>,
        selectedUsersProducer: Producer<Set<User>>,
        private val onUserClicked: (User) -> Unit
) : GroupList<StringGetter, User>(
        context = context,
        fixedSize = true,
        orientation = BaseListOrientation.VERTICAL,
        itemsProducer = DataProducer(users.splitToGroups()),
        itemsViewWrappersCreator = {
            ChooseUsersPageUsersListItemView(
                    context,
                    selectedUsersProducer,
                    onUserClicked
            )
        },
        groupsViewWrappersCreator = { GroupListTitleWrapper(context) }
)