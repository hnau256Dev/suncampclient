package ru.hnau.suncampclient.pages.transactions_list

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.suncamp.common.data.Transaction


class TransactionsListPage(
        private val title: StringGetter,
        private val transactions: List<Transaction>,
        private val relativelyUserLogin: String
): Page<TransactionsListPageView>() {

    override fun generateView(context: Context) =
            TransactionsListPageView(context, this::onBackClicked, title, transactions, relativelyUserLogin)

    private fun onBackClicked() {
        goBack()
    }

}