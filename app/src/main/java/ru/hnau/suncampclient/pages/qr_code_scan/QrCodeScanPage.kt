package ru.hnau.suncampclient.pages.qr_code_scan

import android.content.Context
import android.view.View
import ru.hnau.androidutils.ui.view.pager.Page


class QrCodeScanPage(
        private val afterScan: (String, QrCodeScanPageConnector) -> Unit,
        private val decorationViewBuilder: ((Context, QrCodeScanPageConnector) -> View)? = null
) : Page<QrCodeScanPageView>() {

    override fun generateView(context: Context) =
            QrCodeScanPageView(
                    context = context,
                    afterScan = afterScan,
                    decorationViewBuilder = decorationViewBuilder,
                    showPage = this::showPage,
                    goBack = this::goBack,
                    onBackClicked = this::onBackClicked
            )

    private fun onBackClicked() {
        goBack()
    }

}