package ru.hnau.suncampclient.pages.new_transaction.param

import ru.hnau.jutils.finisher.mapPossible
import ru.hnau.suncamp.common.data.TransactionCategory
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.api.Api
import ru.hnau.suncampclient.pages.new_transaction.TransactionOrNull


class NewStorageToUsersTransactionParam(
        val fromUser: User,
        val toUsers: Set<User>,
        amount: Long? = null,
        category: TransactionCategory = TransactionCategory.encouraging,
        comment: String = ""
) : NewTransactionParam(
        amount,
        category,
        comment
) {

    override fun execute(amount: Long, category: TransactionCategory, comment: String) =
            Api.transactionFromStorageToUsers(
                    fromUserLogin = fromUser.login,
                    comment = comment,
                    amount = amount,
                    category = category,
                    toUsers = toUsers
            ).mapPossible { data -> TransactionOrNull() }

}