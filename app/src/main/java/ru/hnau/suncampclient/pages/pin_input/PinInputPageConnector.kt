package ru.hnau.suncampclient.pages.pin_input

import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.androidutils.ui.view.pager.PagePagerConnector
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer


interface PinInputPageConnector {

    val finishersLockedProducer: FinishersLockedProducer

    fun showPage(
            page: Page<*>,
            clearStack: Boolean = false,
            precedingPages: List<Page<*>> = emptyList()
    ): Boolean

    fun goBack(
            pageResolver: (List<Page<*>>) -> Int? = PagePagerConnector.DEFAULT_GO_BACK_PAGE_RESOLVER
    ): Boolean

    fun clearInput()

    fun setPin(pin: String)

    fun getPin(): String

}