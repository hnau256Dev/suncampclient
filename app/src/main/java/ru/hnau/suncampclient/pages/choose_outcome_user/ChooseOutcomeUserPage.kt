package ru.hnau.suncampclient.pages.choose_outcome_user

import android.content.Context
import ru.hnau.androidutils.ui.view.pager.Page


class ChooseOutcomeUserPage: Page<ChooseOutcomeUserPageView>() {

    override fun generateView(context: Context) =
            ChooseOutcomeUserPageView(context, this::onBackClicked)

    private fun onBackClicked() {
        goBack()
    }

}